import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ISelectOption } from 'src/app/models/select-option.modal';
import { IDatabase } from 'src/app/models/subjects/database.model';
import { ITable } from 'src/app/models/subjects/table.model';
import { IQuestion } from 'src/app/models/tests/question.model';
import { DatabasesService } from 'src/services/subjects-info/databases-service';
import { QuestionsService } from 'src/services/test-info/questions-service';
import { ThemesService } from 'src/services/test-info/themes-service';

@Component({
    templateUrl: 'add-edit-question-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './add-edit-question-modal.component.less'
    ],
})
export class AddEditQuestionModalComponent implements OnInit {
    public questionForm: FormGroup;
    public answerQuery: string;
    public themeOptions: ISelectOption[];
    public databaseOptions: ISelectOption[];

    private databases: IDatabase[];

    public codeMirrorOptions: any = {
        mode: "text/x-mysql",
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        lineWrapping: false,
        extraKeys: { "Ctrl-Space": "autocomplete" },
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        autoCloseBrackets: true,
        matchBrackets: true,
        lint: true
    };


    constructor(
        private questionService: QuestionsService,
        private themesService: ThemesService,
        private databasesService: DatabasesService,
        public dialogRef: MatDialogRef<AddEditQuestionModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IQuestion | null) {
    }

    ngOnInit(): void {
        this.themesService.getAll().subscribe(res => {
            this.themeOptions = res.map(el => {
                return { id: el.id, value: el.name };
            });
        });

        this.databasesService.getFullAll().subscribe(res => {
            this.databases = res;
            this.databaseOptions = res.map(el => {
                return { id: el.id, value: el.name };
            });
        });

        this.initForm();
    }

    public addEditQuestion(): void {
        if (this.questionForm.invalid)
            return;

        const question = { ...this.questionForm.value };
        const questionModel = {
            id: this.data?.id ?? '',
            body: question.question,
            complexity: question.complexity,
            answerBody: question.answer,
            topicId: question.topicId,
            databaseId: question.databaseId
        };

        if (this.data === null)
            this.questionService.add(questionModel).subscribe(_ => { this.close(); });
        else
            this.questionService.update(questionModel).subscribe(_ => { this.close(); });
    }

    public close(): void {
        this.dialogRef.close();
    }

    public isAddButtonDisabled(): boolean {
        return this.questionForm.invalid;
    }

    public changeDatabase(id: string): void {
        const database = this.databases.filter(d => d.id === id)[0];
        this.codeMirrorOptions.mode = 'text/x-' + database.language?.codeMirrorName ?? 'mysql';
        this.codeMirrorOptions.hintOptions = database.tables ? this.toObject(database.tables) : [];
    }

    private toObject(tables: ITable[]): any {
        let tablesObj: any = {};
        tables.forEach(table => {
            tablesObj[table.name] = table.attributes.map(a => a.name);
        });

        let result: any = {};
        result['tables'] = tablesObj;
        return result;
    }

    private initForm(): void {
        const questionBody = this.data === null ? '' : this.data.body;
        const answerBody = this.data === null ? '' : this.data.correctAnswer?.body;
        const complexity = this.data === null ? 1 : this.data.complexity;
        const topicId = this.data === null ? '' : this.data.topicId;
        const databaseId = this.data === null ? '' : this.data.databaseId;

        this.questionForm = new FormGroup({
            question: new FormControl(questionBody, [Validators.required]),
            answer: new FormControl(answerBody, [Validators.required]),
            complexity: new FormControl(complexity, [Validators.required]),
            topicId: new FormControl(topicId, [Validators.required]),
            databaseId: new FormControl(databaseId, [Validators.required])
        });
    }
}
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { QuestionsService } from "src/services/test-info/questions-service";
import { IQuestion } from "../models/tests/question.model";
import { Column } from "../shared/models/table-column";
import { TableModel } from "../shared/models/table-model";
import { AddEditQuestionModalComponent } from "./modals/add-edit-question-modal.component";
import { ConfirmModel, ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";

@Component({
    templateUrl: './questions.component.html',
    styleUrls: [
        '../../styles/styles.less',
        '../../styles/menu.less',
        './questions.component.less'
    ]
})
export class QuestionsComponent implements OnInit {
    public questions: IQuestion[];

    //test  
    public columns: Column[] = [{
        key: 'actions',
        type: 'action',
        isVisible: true
    }]

    //test
    dataSource: MatTableDataSource<TableModel>;

    constructor(private questionsService: QuestionsService, public addQuestionModal: MatDialog, public confirmationModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshQuestions()
    }


    openAdd(): void {
        const dialogRef = this.addQuestionModal.open(AddEditQuestionModalComponent, {
            disableClose: true,
            width: '50%',
            data: null
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshQuestions();
        })
    }

    openEdit(question: IQuestion): void {
        const dialogRef = this.addQuestionModal.open(AddEditQuestionModalComponent, {
            disableClose: true,
            width: '50%',
            data: question
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshQuestions();
        })
    }

    openDelete(question: IQuestion): void {
        const dialogRef = this.confirmationModal.open(ConfirmationModalComponent, {
            maxWidth: '400px',
            disableClose: true,
            data: new ConfirmModel('Confirm action', `Are you sure that you want to delete question?`)
        })
        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.questionsService.delete(question.id).subscribe(_ => {
                    this.refreshQuestions();
                });
            }
        })
    }

    public editColumn(element: TableModel) {
        
    }

    public deleteColumn(element: TableModel) {
        
    }

    private refreshQuestions(): void {
        this.questionsService.getAll().subscribe(questions => {
            this.questions = questions;
        })
    }
}
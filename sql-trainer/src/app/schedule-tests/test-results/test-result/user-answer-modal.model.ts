import { MatTableDataSource } from "@angular/material/table";
import { MatTreeFlatDataSource } from "@angular/material/tree";
import { IQuestion } from "src/app/models/tests/question.model";
import { IScheduleTest } from "src/app/models/tests/schedule-test.model";
import { Column } from "src/app/shared/models/table-column";
import { TableModel } from "src/app/shared/models/table-model";
import { IAttributeNode, INode, ITreeNode } from "src/app/shared/models/tree-node.model";

export interface IUserAnswerModalDto {
    scheduleTestId: string;
    scheduleTest: IScheduleTest | null,
    questionId: string;
    question: IQuestion | null,
    correctAnswerBody: string,
    userId: string;
    body: string;
    programMark: number;
    teacherMark: number;
    answeredAt: Date | null;
    showExecutionResult: boolean;
    table: MatTableDataSource<TableModel>;
    columns: Column[];
    showCorrectAnswerExecutionResult: boolean;
    correctAnswerTable: MatTableDataSource<TableModel>;
    correctAnswerColumns: Column[];
    dataSource: MatTreeFlatDataSource<ITreeNode, INode, INode>;
}
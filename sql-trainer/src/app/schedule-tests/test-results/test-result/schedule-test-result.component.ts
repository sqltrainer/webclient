import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { IScheduleTest } from "src/app/models/tests/schedule-test.model";
import { IUserAnswer } from "src/app/models/tests/user-answer.model";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";
import { AnswersService } from "src/services/test-info/answers-service";
import { QuestionsService } from "src/services/test-info/questions-service";
import { ScheduleTestsService } from "src/services/test-info/schedule-tests-service";
import { TestsService } from "src/services/test-info/tests-service";
import { IUserAnswerModalDto } from "./user-answer-modal.model";
import { DatabasesService } from "src/services/subjects-info/databases-service";
import { MatTableDataSource } from "@angular/material/table";
import { TableModel } from "src/app/shared/models/table-model";
import { Column } from "src/app/shared/models/table-column";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material/tree";
import { FlatTreeControl } from "@angular/cdk/tree";
import { IAttributeNode, IAttributeTreeNode, INode, ITreeNode } from "src/app/shared/models/tree-node.model";
import { forkJoin } from "rxjs";

@Component({
    selector: 'app-schedule-test-result',
    templateUrl: 'schedule-test-result.component.html',
    styleUrls: [
        './schedule-test-result.component.less',
        '../../../../styles/menu.less'
    ]
})
export class ScheduleTestResultComponent implements OnInit {
    
    public userId: string;
    public scheduleTestId: string;
    public codeMirrorOptions: any = {
        mode: "text/x-sql",
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        lineWrapping: false,
        extraKeys: { "Ctrl-Space": "autocomplete" },
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        autoCloseBrackets: true,
        matchBrackets: true,
        lint: true
    };

    private transformer = (node: ITreeNode, level: number) => {
        return {
          expandable: !!node.childrens && node.childrens.length > 0,
          name: node.name,
          level: level,
        };
      }
    public treeControl = new FlatTreeControl<INode>(node => node.level, node => node.expandable);
    public treeFlattener = new MatTreeFlattener(this.transformer, node => node.level, node => node.expandable, node => node.childrens);
    public dataSources: MatTreeFlatDataSource<ITreeNode, INode, INode>[] = [];
    public hasChild = (_: number, node: INode) => node.expandable;

    public answers: IUserAnswerModalDto[];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private testsService: TestsService,
        private answersService: AnswersService,
        private databasesService: DatabasesService,
        private questionsService: QuestionsService,
        private scheduleTestsService: ScheduleTestsService,
        private localStorageService: LocalStorageService) {
    }

    ngOnInit(): void {
        this.scheduleTestId = this.route.snapshot.params['testId'];
        this.userId = this.route.snapshot.params['userId'];

        if (!this.userId) {
            this.answersService.getStudentAnswers(this.scheduleTestId).subscribe(answers => {
                this.answers = this.mapAnswers(answers);
                this.initData();
            });
        } else {
            this.answersService.getUserAnswers(this.userId, this.scheduleTestId).subscribe(answers => {
                this.answers = this.mapAnswers(answers);
                this.initData();
            })
        }
    }

    public getQuestionMaxMark(answer: IUserAnswerModalDto): number {
        if (!answer.scheduleTest?.test || !answer.scheduleTest?.test?.testQuestions)
            return 0.0;

        return answer.scheduleTest.test.testQuestions.filter(tq => tq.questionId === answer.questionId)[0].maxMark;
    }

    public isCheckedByTeacher(scheduleTest: IScheduleTest | null): boolean {
        if (!scheduleTest || !scheduleTest.userScheduleTests)
            return false;
        
        return scheduleTest.userScheduleTests[0].checkedByTeacher ?? false;
    }

    public isStudent(): boolean {
        return this.localStorageService.isStudent();
    }

    public isTeacher(): boolean {
        return this.localStorageService.isTeacher();
    }

    public isAdmin(): boolean {
        return this.localStorageService.isAdmin();
    }

    public goBack(): void {
        if (!this.userId) {
            this.router.navigate(['schedule-tests']);
        } else {
            this.router.navigate(['schedule-test-results', this.scheduleTestId]);
        }
    }

    public setTeacherMark(answer: IUserAnswerModalDto): void {
        this.answersService.setTeacherMark({
            scheduleTestId: answer.scheduleTestId,
            questionId: answer.questionId,
            userId: answer.userId,
            body: answer.body,
            programMark: answer.programMark,
            teacherMark: answer.teacherMark,
            questionMaxMark: 0,
            answeredAt: null,
            question: null
        }).subscribe(_ => {
            // todo: replace it with message showing
            console.log("Success updated");
        })
    }

    public execute(answer: IUserAnswerModalDto, databaseId: string, script: string, isCorrectAnswer: boolean): void {
        this.databasesService.execute(databaseId, script).subscribe(data => {
            const executionData = JSON.parse(data.result);
            if (executionData.length <= 0) {
                if (isCorrectAnswer) {
                    answer.showCorrectAnswerExecutionResult = false;
                } else {
                    answer.showExecutionResult = false;
                }
            }

            if (isCorrectAnswer) {
                answer.correctAnswerColumns = Object.keys(executionData[0]).map(key => {
                    return new Column(key, 'string', true);
                });

                answer.correctAnswerTable = new MatTableDataSource(executionData.map((element: any) => {
                    return new TableModel(element, false, false, false);
                }));

                answer.showCorrectAnswerExecutionResult = true;
            } else {
                answer.columns = Object.keys(executionData[0]).map(key => {
                    return new Column(key, 'string', true);
                });

                answer.table = new MatTableDataSource(executionData.map((element: any) => {
                    return new TableModel(element, false, false, false);
                }));

                answer.showExecutionResult = true;
            }
        }, err => {
            if (isCorrectAnswer) {
                answer.showCorrectAnswerExecutionResult = false;
            } else {
                answer.showExecutionResult = false;
            }
        });
    }

    private mapAnswers(answers: IUserAnswer[]): IUserAnswerModalDto[] {
        const models = answers.map(a => {
            return {
                scheduleTestId: a.scheduleTestId,
                scheduleTest: null,
                questionId: a.questionId,
                question: a.question,
                correctAnswerBody: '',
                userId: a.userId,
                body: a.body,
                programMark: a.programMark,
                teacherMark: a.teacherMark,
                answeredAt: a.answeredAt,
                showExecutionResult: false,
                showCorrectAnswerExecutionResult: false,
                table: new MatTableDataSource<TableModel>([]),
                columns: [],
                correctAnswerTable: new MatTableDataSource<TableModel>([]),
                correctAnswerColumns: [],
                dataSource: new MatTreeFlatDataSource(this.treeControl, this.treeFlattener)
            }
        });

        forkJoin(answers.map(a => this.databasesService.getFullById(a.question?.databaseId ?? ''))).subscribe(databases => {
            for (let i in answers) {
                models[i].dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
                models[i].dataSource.data = [{
                    name: databases[i].name,
                    childrens: !databases[i].tables ? [] : databases[i].tables!.map(table => {
                        return {
                            name: table.name,
                            childrens: table.attributes.map(attribute => {
                                return {
                                    name: attribute.name,
                                    isPrimaryKey: attribute.isPrimaryKey,
                                    isForeignKey: attribute.foreignKeyId !== null,
                                    childrens: null
                                } as IAttributeTreeNode
                            })
                        }
                    })
                }];
            }
        })

        return models;
    }

    private initData(): void {
        this.scheduleTestsService.getById(this.scheduleTestId).subscribe(scheduleTest => {
            for (let answer of this.answers) {
                answer.scheduleTest = scheduleTest;
            }

            this.testsService.getTestById(scheduleTest.testId).subscribe(test => {
                for (let answer of this.answers) {
                    answer.scheduleTest!.test = test;
                }
            })

            this.questionsService.getQuestionsByScheduleTestId(this.scheduleTestId).subscribe(questions => {
                for (let answer of this.answers) {
                    answer.question = questions.filter(q => q.id === answer.questionId)[0];
                }
            });
        });
    }
}
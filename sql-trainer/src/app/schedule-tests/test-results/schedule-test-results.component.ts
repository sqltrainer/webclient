import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IScheduleTest } from 'src/app/models/tests/schedule-test.model';
import { IUserScheduleTest } from 'src/app/models/tests/user-schedule-test.model';
import { ScheduleTestsService } from 'src/services/test-info/schedule-tests-service';
import { TestsService } from 'src/services/test-info/tests-service';
import { UsersService } from 'src/services/users-info/users-service';

@Component({
  selector: 'app-schedule-test-results',
  templateUrl: './schedule-test-results.component.html',
  styleUrls: ['./schedule-test-results.component.less', '../../../styles/menu.less']
})
export class ScheduleTestResultsComponent implements OnInit {
  public scheduleTestId: string;
  public scheduleTest: IScheduleTest;

  constructor(private router: Router, 
    private route: ActivatedRoute,
    private testsService: TestsService,
    private usersService: UsersService,
    private scheduleTestsService: ScheduleTestsService) { }

  ngOnInit(): void {
    this.scheduleTestId = this.route.snapshot.params['id'];
    this.scheduleTestsService.getById(this.scheduleTestId).subscribe(scheduleTest => {
      this.testsService.getTestById(scheduleTest.testId).subscribe(test => {
        scheduleTest.test = test;
        this.scheduleTest = scheduleTest;
        for (let userScheduleTest of scheduleTest.userScheduleTests) {
          this.usersService.getById(userScheduleTest.userId).subscribe(user => {
            userScheduleTest.user = user;
          })
        }
      });
    })

  }

  public goToTestResult(userScheduleTest: IUserScheduleTest): void {
    this.router.navigate(['schedule-test-result', userScheduleTest.scheduleTestId, userScheduleTest.userId ]);
  }

  public goToScheduleTestsPage(): void {
    this.router.navigate(['schedule-tests']);
  }

  public getUserScheduleTests(): IUserScheduleTest[] {
    return this.scheduleTest?.userScheduleTests ?? [];
  }

  public formatDate(date: Date|null): string {
    if (date === null) {
      return '';
    }
    
    const dateTime = new Date(date);
    return this.padLeft(dateTime.getDate(), 2, '0') + '-' +
        this.padLeft(dateTime.getMonth(), 2, '0') + '-' +
        dateTime.getFullYear() + ' ' +
        this.padLeft(dateTime.getHours(), 2, '0') + ':' +
        this.padLeft(dateTime.getMinutes(), 2, '0');
}

private padLeft(value: string | number, length: number, character: string): string {
    return String(value).padStart(length, character);
}
}

import { FlatTreeControl } from "@angular/cdk/tree";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material/tree";
import { Router, ActivatedRoute } from "@angular/router";
import { WebcamImage } from "ngx-webcam";
import { Subject, forkJoin } from "rxjs";
import { IDatabase } from "src/app/models/subjects/database.model";
import { ITable } from "src/app/models/subjects/table.model";
import { IQuestion } from "src/app/models/tests/question.model";
import { IUserAnswer } from "src/app/models/tests/user-answer.model";
import { IUserVerify } from "src/app/models/users/user-verify.model";
import { ConfirmModel, ConfirmationModalComponent } from "src/app/shared/confirmation-modal/confirmation-modal.component";
import { Column } from "src/app/shared/models/table-column";
import { TableModel } from "src/app/shared/models/table-model";
import { IAttributeTreeNode, INode, ITreeNode } from "src/app/shared/models/tree-node.model";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";
import { DatabasesService } from "src/services/subjects-info/databases-service";
import { AnswersService } from "src/services/test-info/answers-service";
import { QuestionsService } from "src/services/test-info/questions-service";
import { ScheduleTestsService } from "src/services/test-info/schedule-tests-service";
import { UsersService } from "src/services/users-info/users-service";

@Component({
    selector: 'app-test',
    templateUrl: 'test.component.html',
    styleUrls: [
        '../../../styles/menu.less',
        './test.component.less'
    ]
})
export class TestComponent implements OnInit, OnDestroy {
    public scheduleTestId: string;
    public testName: string;
    public questions: IQuestion[];
    private questionChecks: boolean[];
    public answerModels: IUserAnswer[];
    public currentQuestionIndex: number = 0;

    public countdownConfig: any = {};

    public readonly imageTrigger: Subject<void> = new Subject<void>();
    private timer: any;
    private verificationFailuresCount: number = 0;

    private transformer = (node: ITreeNode, level: number) => {
        return {
          expandable: !!node.childrens && node.childrens.length > 0,
          name: node.name,
          level: level,
        };
      }

    public treeControl = new FlatTreeControl<INode>(node => node.level, node => node.expandable);
    public treeFlattener = new MatTreeFlattener(this.transformer, node => node.level, node => node.expandable, node => node.childrens);
    public dataSources: MatTreeFlatDataSource<ITreeNode, INode, INode>[] = [];
    public databases: IDatabase[] = [];

    public table: MatTableDataSource<TableModel>;
    public columns: Column[] = [];

    public showExecutionResult: boolean = false;
    public isEmptyResult: boolean = false;
    public executionError: string | null = null;

    public codeMirrorOptions: any = {
        mode: "text/x-mssql",
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        lineWrapping: false,
        extraKeys: { "Ctrl-Space": "autocomplete" },
        gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
        autoCloseBrackets: true,
        matchBrackets: true,
        lint: true
    };

    constructor(private router: Router,
        private route: ActivatedRoute,
        private scheduleTestsService: ScheduleTestsService,
        private questionsService: QuestionsService,
        private answersService: AnswersService,
        private databasesService: DatabasesService,
        private usersService: UsersService,
        private localStorageService: LocalStorageService,
        public confirmationModal: MatDialog) { }

    public ngOnInit(): void {
        this.timer = setInterval(() => {
            this.imageTrigger.next();
        }, 10000)

        this.scheduleTestId = this.route.snapshot.params['id'];
        forkJoin([
            this.scheduleTestsService.getById(this.scheduleTestId),
            this.questionsService.getQuestionsByScheduleTestId(this.scheduleTestId)
        ]).subscribe(results => {
            const scheduleTest = results[0];
            this.testName = scheduleTest.test?.name ?? '';
            const now = new Date();
            const finish = new Date(scheduleTest.finishAt.toLocaleString())
            finish.setSeconds(0);
            this.countdownConfig = {
                leftTime: (finish.getTime() - now.getTime()) / 1000
            }
            this.questions = results[1];
            this.answerModels = this.questions.map((el) => {
                return {
                    scheduleTestId: this.scheduleTestId,
                    questionId: el.id,
                    question: null,
                    userId: '',
                    body: this.localStorageService.getQuestionAnswer(el.id) ?? '',
                    programMark: 0,
                    teacherMark: 0,
                    questionMaxMark: 0,
                    answeredAt: null
                }
            });

            this.questionChecks = this.questions.map(_ => false);
            this.currentQuestionIndex = this.localStorageService.getCurrentQuestionIndex() ?? 0;

            forkJoin(([...new Set(this.questions.map(q => q.databaseId))]).map(id => this.databasesService.getFullById(id))).subscribe(databases => {
                for (let i in this.questions) {
                    const database = databases.filter(d => d.id === this.questions[i].databaseId)[0];
                    this.databases.push(database)
                    this.dataSources.push(new MatTreeFlatDataSource(this.treeControl, this.treeFlattener));
                    this.dataSources[i].data = [{
                        name: database.name,
                        childrens: !database.tables ? [] : database.tables!.map(table => {
                            return {
                                name: table.name,
                                childrens: table.attributes.map(attribute => {
                                    return {
                                        name: attribute.name + (attribute.foreignKeyId ? ` (${attribute.foreignKey?.table.name})` : ''),
                                        isPrimaryKey: attribute.isPrimaryKey,
                                        isForeignKey: attribute.foreignKeyId !== null,
                                        childrens: null
                                    } as IAttributeTreeNode
                                })
                            }
                        })
                    }]
                }

                for (let i in this.questions) {
                    this.questions[i].database = databases[i];
                }

                this.refreshCodeMirrorOptions();
            })
        });
    }

    ngOnDestroy(): void {
        clearInterval(this.timer);
    }

    hasChild = (_: number, node: INode) => node.expandable;
    
    public goToNextQuestion() {
        this.answerModels[this.currentQuestionIndex].answeredAt = new Date();
        this.currentQuestionIndex += 1;
        this.showExecutionResult = false;
        this.localStorageService.setCurrentQuestionIndex(this.currentQuestionIndex);
        //this.refreshCodeMirrorOptions();
    }

    public goToPreviousQuestion() {
        this.answerModels[this.currentQuestionIndex].answeredAt = new Date();
        this.currentQuestionIndex -= 1;
        this.showExecutionResult = false;
        this.localStorageService.setCurrentQuestionIndex(this.currentQuestionIndex);
        //this.refreshCodeMirrorOptions();
    }

    public goToQuestion(index: number) {
        this.currentQuestionIndex = index;
        this.showExecutionResult = false;
        this.localStorageService.setCurrentQuestionIndex(this.currentQuestionIndex);
        //this.refreshCodeMirrorOptions();
    }

    public goToTests() {
        this.router.navigate(['schedule-tests']);
    }

    public editText(newValue: string, questionIndex: number): void {
        this.answerModels[questionIndex].body = newValue;
        this.questionChecks[questionIndex] = false;
        this.localStorageService.addQuestionAnswer(this.questions[questionIndex].id, newValue);
    }

    public checkQuery(question: IQuestion, questionIndex: number): void {
        const answer = this.answerModels[questionIndex].body;
        this.answersService.checkQuery(question.id, answer).subscribe(result => {
            this.questionChecks[questionIndex] = result.result;
        });
    }

    public checkAll(): void {
        for (let questionIndex in this.questions) {
            this.answersService.checkQuery(this.questions[questionIndex].id, this.answerModels[questionIndex].body).subscribe(result => {
                this.questionChecks[questionIndex] = result.result;
            });
        }
    }

    public areAllQuestionsChecked(): boolean {
        return this.questionChecks.filter(qc => !qc).length <= 0;
    }

    public isChecked(questionIndex: number): boolean {
        return this.questionChecks[questionIndex];
    }

    public execute(): void {
        this.showExecutionResult = false;
        this.columns = [];
        this.executionError = null
        
        this.databasesService.execute(this.questions[this.currentQuestionIndex].databaseId, this.answerModels[this.currentQuestionIndex].body).subscribe(data => {
            const executionData = JSON.parse(data.result);
            if (executionData.length <= 0) {
                this.isEmptyResult = true;
                this.showExecutionResult = true;
            }

            this.columns = Object.keys(executionData[0]).map(key => {
                return new Column(key, 'string', true);
            });

            this.table = new MatTableDataSource(executionData.map((element: any) => {
                return new TableModel(element, false, false, false);
            }));

            this.showExecutionResult = true;
            this.questionChecks[this.currentQuestionIndex] = true;
        }, err => {
            this.executionError = err.error;
        });
    }

    public submit(): void {
        const dialogRef = this.confirmationModal.open(ConfirmationModalComponent, {
            maxWidth: '400px',
            disableClose: true,
            data: new ConfirmModel('Confirm action', 'Are you sure?')
        });

        dialogRef.afterClosed().subscribe(result => {
            if (!result)
                return;

            const dto = {
                scheduleTestId: this.scheduleTestId,
                userId: null,
                finishedAt: new Date(),
                checkedByTeacher: null
            };
            const answers = this.answerModels.map(a => {
                a.answeredAt = new Date();
                return a;
            });

            forkJoin([
                this.scheduleTestsService.updateFinished(dto),
                this.answersService.addAnswers(answers)
            ]).subscribe(_ => {
                this.router.navigate(['schedule-tests']);
            });
        })
    }

    public captureImage(webcamImage: WebcamImage): void {
        let imageAsUrl = webcamImage.imageAsDataUrl;
        let imageAsBase64 = webcamImage.imageAsBase64;
    
        let userVerify: IUserVerify = {
            base64: imageAsBase64,
            extension: this.getImageExtension(imageAsUrl)
        };
    
        this.usersService.verifyUser(userVerify).subscribe(res => {
            if (!res.verified && res.errors) {
                alert(res.errors.join('/n'))
                this.localStorageService.saveCountOfVerificationFails(this.verificationFailuresCount++);

                const lsCountOfVerificationFailures = this.localStorageService.getCountOfVerificationFails()
                if (3 < this.verificationFailuresCount || lsCountOfVerificationFailures && 3 < lsCountOfVerificationFailures) {
                    const dto = {
                        scheduleTestId: this.scheduleTestId,
                        userId: null,
                        finishedAt: new Date(),
                        checkedByTeacher: null
                    };
                
                    this.scheduleTestsService.updateFinished(dto).subscribe(_ => {
                        this.router.navigate(['schedule-tests']);
                    })
                }
            }
        });
    }

    private getImageExtension(imageAsDataUrl: string): string {
        let indexOfSlash = imageAsDataUrl.indexOf('/');
        let indexOfSemiColon = imageAsDataUrl.indexOf(';');
        return imageAsDataUrl.slice(indexOfSlash, indexOfSemiColon).replace('/', '.');
    }

    private refreshCodeMirrorOptions(): void {
        const database = this.questions[this.currentQuestionIndex].database;
        this.codeMirrorOptions.hintOptions = database?.tables ? this.toObject(database.tables) : [];
    }

    private toObject(tables: ITable[]): any {
        let tablesObj: any = {};
        tables.forEach(table => {
            tablesObj[table.name] = table.attributes.map(a => a.name);
        });

        let result: any = {};
        result['tables'] = tablesObj;
        return result;
    }
}
import { AfterViewInit, Component, Inject, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { ISelectOption } from "src/app/models/select-option.modal";
import { TestsService } from "src/services/test-info/tests-service";
import { GroupsService } from "src/services/users-info/groups-service";
import { UsersService } from "src/services/users-info/users-service";
import { ErrorStateMatcher, ThemePalette } from '@angular/material/core';
import { IUser } from "src/app/models/users/user.model";
import { IScheduleTest } from "src/app/models/tests/schedule-test.model";
import { ScheduleTestsService } from "src/services/test-info/schedule-tests-service";
import { IScheduleTestDto } from "src/app/models/tests/schedule-test.dto";
import * as moment from 'moment';

@Component({
    templateUrl: './add-edit-schedule-test-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './add-edit-schedule-test-modal.component.less'
    ]
})
export class AddEditScheduleTestModalComponent implements OnInit, AfterViewInit {
    public addEditForm: FormGroup;
    public testOptions: ISelectOption[];
    public groupOptions: ISelectOption[];
    public userOptions: ISelectOption[];

    public minDate: any;
    public color: ThemePalette = 'primary';
    public matcher = new DateErrorStateMatcher();

    private users: IUser[];
    private selectedGroupIds: string[] = [];

    constructor(
        private testsService: TestsService,
        private scheduleTestsService: ScheduleTestsService,
        private groupsService: GroupsService,
        private usersService: UsersService,
        public dialogRef: MatDialogRef<AddEditScheduleTestModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IScheduleTest) {

        this.minDate = moment(new Date());
    }

    ngAfterViewInit(): void {
        this.checkDates(this.addEditForm.get('dates') as AbstractControl)
    }

    ngOnInit(): void {
        this.initForm();
        this.testsService.getAll().subscribe(tests => {
            this.testOptions = tests.map(test => {
                return { id: test.id, value: test.name };
            });
        });

        this.groupsService.getAll().subscribe(groups => {
            this.groupOptions = groups.map(group => {
                return { id: group.id, value: group.name };
            });
        });

        this.usersService.getStudents().subscribe(students => {
            this.users = students;
            this.selectedGroupIds = [...new Set(students.filter(s => this.data?.userScheduleTests.map(ust => ust.userId).includes(s.id!)).map(s => s.groupId!))]
            this.addEditForm.get('groupIds')?.setValue(this.selectedGroupIds);
            this.userOptions = students.map(student => {
                return { id: student.id ?? '', value: student.name };
            });
        });        
    }

    public close(): void {
        this.dialogRef.close();
    }

    public addEdit(): void {
        const testUsers = { ...this.addEditForm.value };
        const dto = {
            id: this.data?.id ?? '00000000-0000-0000-0000-000000000000',
            userIds: testUsers.userIds,
            testId: this.data?.testId ?? testUsers.testId,
            startAt: testUsers.dates.startAt.toDate(),
            finishAt: testUsers.dates.finishAt.toDate()
        } as IScheduleTestDto

        if (this.data === null) {
            this.scheduleTestsService.add(dto).subscribe(_ => {
                this.close();
            });
        } else {
            this.scheduleTestsService.update(dto).subscribe(_ => {
                this.close();
            })
        }
    }

    public chooseGroup(groupIds: string[]): void {
        
        if (groupIds.length < this.selectedGroupIds.length){
            // To remove
            const groupId = this.selectedGroupIds.filter(sg => !groupIds.includes(sg))[0];
            const userIds = this.addEditForm.get('userIds')?.value;
            const userGroupIds = this.users.filter(u => u.groupId === groupId).map(u => u.id);
            const final = userIds.filter((uId: string) => !userGroupIds.includes(uId));
            this.addEditForm.get('userIds')?.setValue(final);
        } else {
            // To add
            const groupId = groupIds.filter(sg => !this.selectedGroupIds.includes(sg))[0];
            const userIds = this.addEditForm.get('userIds')?.value;
            for (let userId of this.users.filter(u => u.groupId === groupId).map(u => u.id)) {
                userIds.push(userId)
            }
            this.addEditForm.get('userIds')?.setValue(userIds);
        }

        this.selectedGroupIds = structuredClone(groupIds);
    }

    public showError() {
        let a = this.addEditForm.get('dates')?.get('finishAt')?.getError('datesError');
        return a;
    }

    public isButtonDisabled() {
        return !!this.addEditForm?.invalid;
    }

    private initForm(): void {
        this.addEditForm = new FormGroup({
            testId: new FormControl(this.data?.testId ?? '', [Validators.required]),
            userIds: new FormControl(this.data?.userScheduleTests?.map(ust => ust.userId) ?? [], [Validators.required]),
            groupIds: new FormControl(this.selectedGroupIds, []),
            dates: new FormBuilder().group({
                startAt: new FormControl(moment(this.data?.startAt) ?? moment(new Date()), [Validators.required]),
                finishAt: new FormControl(moment(this.data?.finishAt) ?? moment(new Date()), [Validators.required])
            }, { validator: this.checkDates })
        });
    }

    private checkDates(group: AbstractControl) {
        let startAt = new Date(group.get('startAt')?.value);
        let finishAt = new Date(group.get('finishAt')?.value);
        if (startAt >= finishAt) {
            group.get('finishAt')?.setErrors({ datesError: true });
            group.setErrors({ datesError: true });
        }
    }
}

export class DateErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
      const isSubmitted = !!form && form.submitted;
      return (!!control && control.invalid && !isSubmitted);
    }
  }
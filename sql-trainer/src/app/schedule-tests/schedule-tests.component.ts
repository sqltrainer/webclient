import { Component, OnInit } from "@angular/core";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";
import { MatDialog } from "@angular/material/dialog";
import { AddEditScheduleTestModalComponent } from "./modals/add-edit-schedule-test-modal.component";
import { ConfirmModel, ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";
import { Router } from "@angular/router";
import { IScheduleTest } from "../models/tests/schedule-test.model";
import { ScheduleTestsService } from "src/services/test-info/schedule-tests-service";
import { FaceVerificationModalComponent } from "../shared/face-verification-modal/face-verification-modal.component";

@Component({
    templateUrl: './schedule-tests.component.html',
    styleUrls: [
        '../../styles/menu.less',
        './schedule-tests.component.less'
    ]
})
export class ScheduleTestsComponent implements OnInit {
    public tests: IScheduleTest[];

    constructor(
        private localStorageService: LocalStorageService,
        private scheduleTestsService: ScheduleTestsService,
        private router: Router,
        public addEditScheduleTestModal: MatDialog,
        public confimationModal: MatDialog,
        public faceVerificationModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshScheduleTests();
    }

    public openAdd(): void {
        const dialogRef = this.addEditScheduleTestModal.open(AddEditScheduleTestModalComponent, {
            disableClose: true
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshScheduleTests();
        });
    }

    public openEdit(test: IScheduleTest): void {
        const dialogRef = this.addEditScheduleTestModal.open(AddEditScheduleTestModalComponent, {
            disableClose: true,
            data: test
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshScheduleTests();
        });
    }

    public openDelete(test: IScheduleTest): void {
        const dialogRef = this.confimationModal.open(ConfirmationModalComponent, {
            data: new ConfirmModel('Confirm action', `Are you sure you want to delete schedule test: ${test.test?.name}`)
        });

        dialogRef.afterClosed().subscribe(res => {
            if (res) {
                this.scheduleTestsService.delete(test.id).subscribe(_ => {
                    this.refreshScheduleTests();
                })
            }
        })
    }

    public goToTestPage(scheduleTestId: string): void {
        const dialogRef = this.faceVerificationModal.open(FaceVerificationModalComponent);
        dialogRef.afterClosed().subscribe(res => {
            if (!!res) {
                this.router.navigate(['test', scheduleTestId]);
            }
        })
    }

    public goToCheckResultPage(scheduleTestId: string): void {
        this.isAdmin() || this.isTeacher() ?
            this.router.navigate(['schedule-test-results', scheduleTestId]) :
            this.router.navigate(['schedule-test-result', scheduleTestId]);

    }

    public isStudent(): boolean {
        return this.localStorageService.isStudent();
    }

    public isAdmin(): boolean {
        return this.localStorageService.isAdmin();
    }

    public isTeacher(): boolean {
        return this.localStorageService.isTeacher();
    }

    public isTestActive(test: IScheduleTest): boolean {
        const now = new Date();
        const start = new Date(test.startAt.toLocaleString())
        const finish = new Date(test.finishAt.toLocaleString())

        now.setSeconds(0);
        start.setSeconds(0);
        finish.setSeconds(0);

        return start <= now && now <= finish;
    }

    public formatDate(date: Date): string {
        const dateTime = new Date(date);
        return this.padLeft(dateTime.getDate(), 2, '0') + '-' +
            this.padLeft(dateTime.getMonth() + 1, 2, '0') + '-' +
            dateTime.getFullYear() + ' ' +
            this.padLeft(dateTime.getHours(), 2, '0') + ':' +
            this.padLeft(dateTime.getMinutes(), 2, '0');
    }

    private padLeft(value: string | number, length: number, character: string): string {
        return String(value).padStart(length, character);
    }

    private refreshScheduleTests(): void {
        if (this.localStorageService.isStudent()) {
            this.scheduleTestsService.getAllByUserId().subscribe(scheduleTests => {
                this.tests = scheduleTests;
            });
        } else {
            this.scheduleTestsService.getAll().subscribe(scheduleTests => {
                this.tests = scheduleTests;
            });
        }
    }
}
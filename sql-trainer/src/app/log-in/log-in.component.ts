import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material/dialog';

import { UsersService } from '../../services/users-info/users-service';
import { LocalStorageService } from 'src/services/local-storage/local-storage-service';

@Component({
    templateUrl: 'log-in.component.html',
    styleUrls: [
        '../../styles/dialog.less',
        './log-in.component.less'
    ]
})
export class LogInDialogComponent implements OnInit {
    public loginForm: FormGroup;
    
    constructor(private usersService: UsersService, private localStorageService: LocalStorageService, public dialogRef: MatDialogRef<LogInDialogComponent>) { }

    ngOnInit(): void {
        this.initForm();
    }

    public logIn(): void {
        if (this.loginForm.invalid)
            return;
        
        let loginDto = {...this.loginForm.value};
        this.usersService.logIn(loginDto).subscribe(tokenDto => {
            this.localStorageService.saveToken(tokenDto.token);
            this.localStorageService.saveRole(tokenDto.role);
            this.usersService.loginStatus.next({ login: loginDto.login });
            this.cancel();
        })
    }

    public cancel(): void {
        this.dialogRef.close();
    }

    public isLoginButtonDisabled(): boolean {
        return this.loginForm.invalid;
    }

    private initForm(): void {
        this.loginForm = new FormGroup({
            login: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required, Validators.minLength(6)])
        });
    }
}
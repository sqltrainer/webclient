import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";

import { MatDialog } from '@angular/material/dialog';
import { UsersService } from "src/services/users-info/users-service";
import { LogInDialogComponent } from "../log-in/log-in.component";
import { SignInDialogComponent } from "../sign-in/sign-in.component";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";

@Component({
    selector: 'header',
    templateUrl: 'header.component.html',
    styleUrls: [
        '../../styles/styles.less',
        'header.component.less'
    ]
})
export class HeaderComponent implements OnInit, OnDestroy {
    @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

    loginStatusSubscription: any;

    isLoggedIn: boolean = false;
    login: string;

    constructor(private userService: UsersService, private localStorageService: LocalStorageService, public dialog: MatDialog) { }

    ngOnInit(): void {
        if (this.checkIsLoggedIn()) {
            this.userService.getUser().subscribe(userModel => {
                this.isLoggedIn = true;
                this.login = userModel.login;
            })
        }

        this.loginStatusSubscription = this.userService.getLoginStatusEvent().subscribe(loginModel => {
            if (loginModel.login && this.checkIsLoggedIn()) {
                this.isLoggedIn = true;
                this.login = loginModel.login;
            }
        })
    }

    openLogInDialog(): void {
        const dialogRef = this.dialog.open(LogInDialogComponent);
        dialogRef;
    }

    openSignInDialog(): void {
        const dialogRef = this.dialog.open(SignInDialogComponent);
        dialogRef;
    }

    logOut(): void {
        this.isLoggedIn = false;
        this.localStorageService.clearLocalStorage();
    }

    toggleSideBar() {
        this.toggle.emit();
    }

    ngOnDestroy(): void {
        this.loginStatusSubscription.unsubscribe();
    }

    checkIsLoggedIn(): boolean {
        return this.localStorageService.getToken() !== null;
    }
}
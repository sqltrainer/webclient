import { Component, OnInit } from "@angular/core";
import { IGroup } from "../models/users/group.model";
import { GroupsService } from "src/services/users-info/groups-service";
import { AddEditGroupModalComponent } from "./modals/add-edit-group-modal.component";
import { ConfirmModel, ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
    templateUrl: './groups.component.html',
    styleUrls: [
        '../../styles/menu.less',
        './groups.component.less'
    ]
})
export class GroupsComponent implements OnInit {
    public groups: IGroup[];

    constructor(
        private groupsService: GroupsService,
        public addEditGroupModal: MatDialog,
        public confirmationModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshGroups();
    }

    public openAdd(): void {
        const dialogRef = this.addEditGroupModal.open(AddEditGroupModalComponent, {
            disableClose: true,
            data: null
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshGroups();
        })
    }

    public openEdit(group: IGroup): void {
        const dialogRef = this.addEditGroupModal.open(AddEditGroupModalComponent, {
            disableClose: true,
            data: group
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshGroups();
        })
    }

    public openDelete(group: IGroup): void {
        const dialogRef = this.confirmationModal.open(ConfirmationModalComponent, {
            maxWidth: '400px',
            disableClose: true,
            data: new ConfirmModel('Confirm action', `Are you sure that you want to delete "${group.name}" test?`)
        })
        dialogRef.afterClosed().subscribe(confirmed => {
            if (confirmed) {
                this.groupsService.delete(group).subscribe(_ => { this.refreshGroups(); });
            }
        })
    }

    private refreshGroups(): void {
        this.groupsService.getAll().subscribe(groups => {
            this.groups = groups;
        });
    }
}
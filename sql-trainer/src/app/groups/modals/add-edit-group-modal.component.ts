import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { ISelectOption } from "src/app/models/select-option.modal";
import { IGroup } from "src/app/models/users/group.model";
import { UsersService } from "src/services/users-info/users-service";
import { GroupsService } from "src/services/users-info/groups-service";

@Component({
    templateUrl: './add-edit-group-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './add-edit-group-modal.component.less'
    ]
})
export class AddEditGroupModalComponent implements OnInit {
    public addEditForm: FormGroup;
    public studentOptions: ISelectOption[];

    constructor(
        private usersService: UsersService,
        private groupsService: GroupsService,
        public dialogRef: MatDialogRef<AddEditGroupModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IGroup | null) {
    }

    ngOnInit(): void {
        this.initForm();
    }

    public isAddButtonDisabled(): boolean {
        return this.addEditForm.invalid;
    }

    public addEditGroup(): void {
        if (this.addEditForm.invalid)
            return;

        const group = { ...this.addEditForm.value };
        const groupModel = {
            id: this.data?.id ?? '',
            name: group.name
        };

        if (this.data === null)
            this.groupsService.add(groupModel).subscribe(_ => { this.close(); });
        else
            this.groupsService.update(groupModel).subscribe(_ => { this.close(); });
    }

    public close(): void {
        this.dialogRef.close();
    }

    private initForm(): void {
        this.addEditForm = new FormGroup({
            name: new FormControl(this.data?.name ?? '', [Validators.required])
        });
    }
}
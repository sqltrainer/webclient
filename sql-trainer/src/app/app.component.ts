import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'sql-trainer';
  public isOpened: boolean = true;
  
  public toggleSideBar() {
    this.isOpened = !this.isOpened;
  }
}

import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { Column } from "src/app/shared/models/table-column";
import { TableModel } from "src/app/shared/models/table-model";
import { AddSubjectTableDataModelComponent } from "../modals/add-subject-table-data-modal/add-subject-table-data-modal.component";
import { ITable } from "src/app/models/subjects/table.model";
import { TablesService } from "src/services/subjects-info/tables-service";
import { IAttribute } from "src/app/models/subjects/attribute.model";
import { EditTableModel } from "src/app/shared/models/edit-table-model";

@Component({
    selector: 'subject-table-data',
    templateUrl: './subject-table-data.component.html',
    styleUrls: [
        './subject-table-data.component.less',
        '../../../styles/styles.less',
        '../../../styles/menu.less',
    ]
})
export class SubjectTableData implements OnInit {
    private tableId: string;
    private table: ITable;
    private attributes: IAttribute[];
    public data: any[] = [
    ]
    public subjectTableData: TableModel[];
    public columns: Column[] = [
        { key: 'Actions', type: 'action', isVisible: true }
    ]
    public dataSource: MatTableDataSource<TableModel>;

    constructor(
        private tablesService: TablesService, 
        private router: Router, 
        private route: ActivatedRoute, 
        private matDialog: MatDialog) { }

    ngOnInit(): void {
        this.tableId = this.route.snapshot.params['id'];
        this.refreshData();

        this.tablesService.getById(this.tableId).subscribe(table => {
            this.table = table;
        });
    }

    public goToSubjectTables(): void {
        this.router.navigate(['subjects', this.table.databaseId]);
    }

    public openAdd(): void {
        const dialogRef = this.matDialog.open(AddSubjectTableDataModelComponent);

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshData();
        })

        let obj: any = {};
        this.attributes.forEach(attribute => {
            obj[attribute.name] = this.mapDefaultTypeValue(attribute.type)
        });

        dialogRef.componentInstance.tableId = this.tableId;
        dialogRef.componentInstance.model = obj;
        dialogRef.componentInstance.attributes = this.attributes;
    }

    public delete(element: TableModel): void {
        const data = { jsonData: JSON.stringify(element.data) }
        this.tablesService.deleteData(this.tableId, data).subscribe(_ => {
            this.refreshData();
        })
    }

    public update(element: TableModel): void {
        const editElement = structuredClone(element as EditTableModel);
        element.data = editElement.oldData;
        const data = { jsonData: JSON.stringify(editElement.data), oldJsonData: JSON.stringify(editElement.oldData) }
        this.tablesService.updateData(this.tableId, data).subscribe(_ => {
            this.refreshData();
        })
    }

    private initTable() {
        if (this.columns.length <= 1) {
            this.tablesService.getAttributesByTableId(this.tableId).subscribe(attributes => {
                this.attributes = attributes;
                this.columns.unshift(...attributes.map(attribute => {
                    return new Column(attribute.name, this.mapType(attribute.type), true);
                }));
                this.setDate();
            })
        }

        this.subjectTableData = this.data.map(el => {
            return new TableModel(el, false, false);
        });

        this.dataSource = new MatTableDataSource(this.subjectTableData);
    }

    private refreshData(): void {
        this.tablesService.getDataByTableId(this.tableId).subscribe(data => {
            this.data = JSON.parse(data.jsonData);
            this.initTable();
            this.setDate();
        })
    }

    private mapDefaultTypeValue(type: string): any {
        switch (type) {
            case 'varchar':
            case 'uniqueidentifier':
                return '';
            case 'int':
            case 'real':
            case 'float':
            case 'decimal':
                return 0;
            case 'bit':
                return false;
            // case 'datetime':
            //     return new Date();
            default:
                return '';
        }
    }

    private mapType(dbType: string): string {
        switch (dbType) {
            case 'varchar':
            case 'uniqueidentifier':
                return 'string';
            case 'bit':
                return 'boolean';
            case 'int':
            case 'real':
            case 'float':
            case 'decimal':
                return 'number';
            case 'datetime':
                return 'date';
            default:
                return 'string';
        }
    }

    private setDate(): void {
        for (let element of this.data) {
            for (let column of this.columns) {
                if (column.type === 'date') {
                    const date = element[column.key];

                    const month = date.substring(0, 2);
                    const day = date.substring(2, 4);
                    const year = date.substring(4, 8);
                    let hours = parseInt(date.substring(8, 10));
                    const minutes = date.substring(10, 12);
                    const timezone = date.substring(12);

                    if (timezone.substring(0, 1) === '+') {
                        hours += parseInt(timezone.substring(1, 3));
                    } else {
                        hours -= parseInt(timezone.substring(1, 3));
                    }

                    element[column.key] = new Date(year, month - 1, day, hours, minutes);
                }
            }
        }
    }

    public formatDate(date: Date): string {
        const dateTime = new Date(date);
        return this.padLeft(dateTime.getDate(), 2, '0') + '-' +
            this.padLeft(dateTime.getMonth(), 2, '0') + '-' +
            dateTime.getFullYear() + ' ' +
            this.padLeft(dateTime.getHours(), 2, '0') + ':' + 
            this.padLeft(dateTime.getMinutes(), 2, '0');
    }

    private padLeft(value: string|number, length: number, character: string): string {
        return String(value).padStart(length, character);
    }
}
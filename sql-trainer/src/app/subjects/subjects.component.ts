import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { Router } from "@angular/router";
import { Column } from "../shared/models/table-column";
import { TableModel } from "../shared/models/table-model";
import { AddSubjectModalComponent } from "./modals/add-subject-modal/add-subject-modal.component";
import { IDatabase } from "../models/subjects/database.model";
import { DatabasesService } from "src/services/subjects-info/databases-service";

@Component({
    selector: 'subjects',
    templateUrl: './subjects.component.html',
    styleUrls: ['./subjects.component.less',
        '../../styles/styles.less',
        '../../styles/menu.less',]
})
export class SubjectsComponent implements OnInit {
    private isLoadedDatabases: boolean = false;
    public databases: IDatabase[];

    public columns: Column[];

    public dataSource: MatTableDataSource<TableModel>;

    constructor(private databasesService: DatabasesService, private router: Router, public addSubjectModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshColumns();
        this.refreshDatabases();
    }

    public getColumnsKeys() {
        return this.columns.map(el => {
            return el.key;
        })
    }

    public edit(element: TableModel): void {
        if (!confirm('Are you sure that you want to rename the database?'))
            return;

        const dto = {
            id: element.data.id,
            name: element.data.name
        }
        this.databasesService.update(dto).subscribe(_ => {
            this.refreshDatabases();
        });
    }

    public delete(element: TableModel): void {
        if (!confirm('Are you sure that you want to delete the database?'))
            return;

        this.databasesService.delete(element.data.id).subscribe(_ => {
            this.refreshColumns();
            this.isLoadedDatabases = false;
            this.refreshDatabases();
        });
    }

    public showMoreDbInfo(element: TableModel) {
        this.router.navigate(['/subjects', element.data.id])
    }

    public openAddSubjectModal() {
        const dialogRef = this.addSubjectModal.open(AddSubjectModalComponent, {
            width: '450px'
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshColumns();
            this.isLoadedDatabases = false;
            this.refreshDatabases();
        })
    }

    private refreshColumns(): void {
        this.columns = [{ key: 'Actions', type: 'action', isVisible: true }];
    }

    private refreshDatabases(): void {
        this.databasesService.getAll().subscribe(databases => {
            this.databases = databases;
            
            this.dataSource = new MatTableDataSource(databases.map(db => {
                return new TableModel(db, false, true);
            }));

            if (!this.isLoadedDatabases) {
                this.mapDataColumns();
                this.isLoadedDatabases = true;
            }
        })
    }

    private mapDataColumns() {
        if (this.databases.length <= 0)
            return;
        
        const db = this.databases[0] as any;
        this.columns.unshift(...Object.keys(db).map(key => {
            return new Column(key, typeof db[key], !key.includes('id') && !key.includes('Id'));
        }))
    }
}
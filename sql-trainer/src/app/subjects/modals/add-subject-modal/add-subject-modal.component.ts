import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { IDatabase } from "src/app/models/subjects/database.model";
import { DatabasesService } from "src/services/subjects-info/databases-service";

@Component({
    selector: 'add-subject-modal',
    templateUrl: './add-subject-modal.component.html',
    styleUrls: [
        './add-subject-modal.component.less',
        '../../../../styles/dialog.less',
        '../../../../styles/styles.less'
    ]
})

export class AddSubjectModalComponent implements OnInit {
    public addForm: FormGroup; 

    constructor(private databasesService: DatabasesService, public dialogRef: MatDialogRef<AddSubjectModalComponent>) { }

    ngOnInit(): void {
        this.initForm();
    }
    
    public close(): void {
        this.dialogRef.close();
    }

    public add(): void {
        if (this.addForm.invalid)
            return;

        const model = { ...this.addForm.value };
        this.databasesService.add({ name: model.subjectName, languageId: 'f0c9b7a9-8a1f-4c4c-8e3a-5f8b2d6c0f6e' }).subscribe(_ => {
            this.close();
        })
    }

    private initForm() {
        this.addForm = new FormGroup({
            subjectName: new FormControl('', [Validators.required])
        });
    }
}
import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { IAttributeAddModalDto } from "src/app/models/subjects/attribute.add-modal.dto";
import { IAttribute } from "src/app/models/subjects/attribute.model";
import { Column } from "src/app/shared/models/table-column";
import { TableModel } from "src/app/shared/models/table-model";
import { DatabasesService } from "src/services/subjects-info/databases-service";
import { TablesService } from "src/services/subjects-info/tables-service";

@Component({
    selector: 'add-subject-table-modal',
    templateUrl: './add-edit-subject-table-modal.component.html',
    styleUrls: [
        './add-edit-subject-table-modal.component.less',
        '../../../../styles/dialog.less'
    ]
})

export class AddEditSubjectTableModalComponent implements OnInit {
    public addForm: FormGroup;

    public databaseId: string;

    public dataSource: MatTableDataSource<TableModel>;
    public displayedColumns: Column[] = [{ key: 'Actions', type: 'action', isVisible: true }];

    constructor(private tablesService: TablesService,
        private databasesService: DatabasesService,
        public dialogRef: MatDialogRef<AddEditSubjectTableModalComponent>, @Inject(MAT_DIALOG_DATA) private data: any) {
    }

    ngOnInit(): void {
        this.initForm();
        this.initTableData();
    }

    public addEdit(): void {
        const table = { ...this.addForm.value };

        const dto = {
            id: this.data?.id ?? null,
            name: table.tableName,
            databaseId: this.databaseId,
            attributes: this.dataSource.data.map(attributeModel => {
                const attribute = attributeModel.data;
                return {
                    id: this.data !== null && typeof this.data !== 'string' && !attribute.id ? '00000000-0000-0000-0000-000000000000' : attribute.id ?? null,
                    tableId: this.data?.id ?? null,
                    name: attribute.name,
                    type: attribute.type,
                    varcharNumberOfSymbols: attribute.varcharNumber ? attribute.varcharNumber : null,
                    isPrimaryKey: attribute.primaryKey,
                    isNotNull: attribute.notNull,
                    isUnique: attribute.unique,
                    defaultValue: attribute.default,
                    foreignKeyId: attribute.foreignKeyId ? attribute.foreignKeyId : null,
                    order: attribute.order
                }
            })
        };

        if (this.data === null || typeof this.data === 'string') {
            this.tablesService.add(dto).subscribe(_ => {
                this.close();
            });
        } else {
            this.tablesService.update(dto).subscribe(_ => {
                this.close();
            })
        }
    }

    public showAddRow(): void {
        const clearRow = this.getClearRow()

        const newModel = new TableModel(clearRow, false, false, true);

        this.dataSource.data = [...this.dataSource.data, newModel];
    }

    public deleteRow(element: TableModel): void {
        this.dataSource.data = this.dataSource.data.filter(el => el !== element)
    }

    public close(): void {
        this.dialogRef.close();
    }

    private initForm() {
        this.addForm = new FormGroup({
            tableName: new FormControl(this.isAdd() ? '' : this.data.name, [Validators.required])
        });
    }

    private initTableData() {
        this.databasesService.getPrimaryKeysByDatabaseId(this.databaseId).subscribe(res => {
            if (this.isAdd()) {
                const clearRow = this.getClearRow();
                const row = clearRow as any;
                this.displayedColumns.unshift(...Object.keys(row).map(el => {
                    let column = new Column(el, typeof row[el], el !== 'id');
                    if (el === 'foreignKeyId') {
                        column.type = 'select';
                        column.customColumnOptions = res.map(el => {
                            return {
                                id: el.id, value: el.name + ' (' + el.table?.name + ')'
                            };
                        });
                    }
                    return column;
                }));

                let data = [new TableModel(clearRow, false, false, true)];
                this.dataSource = new MatTableDataSource(data);
                return;
            }

            this.tablesService.getAttributesByTableId(this.data.id).subscribe(attributes => {
                const attributesModals = attributes.map((a: IAttribute) => {
                    return {
                        id: a.id,
                        name: a.name,
                        type: a.type,
                        varcharNumber: a.varcharNumberOfSymbols,
                        primaryKey: a.isPrimaryKey,
                        notNull: a.isNotNull,
                        unique: a.isUnique,
                        default: a.defaultValue,
                        foreignKeyId: a.foreignKeyId,
                        order: a.order
                    }
                });

                const attr = attributesModals[0] as any;
                this.displayedColumns.unshift(...Object.keys(attr).map(el => {
                    let column = new Column(el, typeof attr[el], el !== 'id');
                    if (el === 'foreignKeyId') {
                        column.type = 'select';
                        column.customColumnOptions = res.map(el => {
                            return {
                                id: el.id, value: el.name + ' (' + el.table?.name + ')'
                            };
                        });
                    }
                    return column;
                }));

                let data = attributesModals.map((am: IAttributeAddModalDto) => {
                    return new TableModel(am, false)
                })
                this.dataSource = new MatTableDataSource(data);
            });
        });
    }

    private getClearRow(): IAttributeAddModalDto {
        return {
            id: '',
            name: '',
            type: '',
            varcharNumber: null,
            primaryKey: false,
            notNull: false,
            unique: false,
            default: null,
            foreignKeyId: '',
            order: 0
        };
    }

    public isAdd(): boolean {
        return typeof this.data === 'string';
    }
}
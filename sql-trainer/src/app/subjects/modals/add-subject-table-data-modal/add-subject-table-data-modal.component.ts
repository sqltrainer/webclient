import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { ThemePalette } from "@angular/material/core";
import { MatDialogRef } from "@angular/material/dialog";
import { IAttribute } from "src/app/models/subjects/attribute.model";
import { TablesService } from "src/services/subjects-info/tables-service";
import { IKeyType } from "./key-type";
import { IKeySelects } from "./key-selects";

@Component({
    selector: 'add-subject-table-data-modal',
    templateUrl: './add-subject-table-data-modal.component.html',
    styleUrls: [
        './add-subject-table-data-modal.component.less',
        '../../../../styles/dialog.less'
    ]
})

export class AddSubjectTableDataModelComponent implements OnInit {
    public addForm: FormGroup;
    public tableId: string;
    public attributes: IAttribute[];
    public model: any;

    public minDate: Date;
    public color: ThemePalette = 'primary';

    public keyTypes: IKeyType[] = [];

    constructor(
        private tablesService: TablesService,
        public dialogRef: MatDialogRef<AddSubjectTableDataModelComponent>) { }

    ngOnInit(): void {
        this.initFrom();

        for (let key of Object.keys(this.model)) {
            if (this.attributes.filter(a => a.name === key)[0].type === 'datetime') {
                this.keyTypes.push({ key: key, type: 'date', selects: null });
                continue;
            }
    
            const attribute = this.attributes.filter(a => a.name === key)[0];
            if (attribute.foreignKeyId) {
                this.tablesService.getDataByForeignKeyId(attribute.foreignKeyId).subscribe(data => {
                    this.tablesService.getAttributesByPrimaryKey(attribute.foreignKeyId as string).subscribe(attributes => {
                        const primaryKey = attributes.filter(a => a.isPrimaryKey)[0];
                        const rows = JSON.parse(data.jsonData) as any[];
                        this.keyTypes.push({ 
                            key: key, 
                            type: 'fk',
                            selects: rows.map(r => { return { id: r[primaryKey.name], value: JSON.stringify(r) } }) 
                        })
                    })
                })
                continue;
            }
    
            switch(typeof this.model[key]) {
                case 'string': this.keyTypes.push({ key: key, type: 'text', selects: null }); break;
                case 'boolean': this.keyTypes.push({ key: key, type: 'checkbox', selects: null }); break;
                case 'number': this.keyTypes.push({ key: key, type: 'number', selects: null });
            }
        }
    }

    public close() {
        this.dialogRef.close();
    }

    public getInputType(key: string): string {
        switch(typeof this.model[key]) {
            case 'string': return 'text';
            case 'boolean': return 'checkbox';
            case 'number': return 'number';
        }
        return '';
    }

    public isDateTime(key: string): boolean {
        return this.attributes.filter(a => a.name === key)[0].type === 'datetime';
    }

    public add(): void {
        if (this.addForm.invalid)
            return;
            
        this.tablesService.insertDataIntoTable(this.tableId, { jsonData: JSON.stringify({ ...this.addForm.value }) }).subscribe(_ => {
            this.close();
        })
    }

    public checkForNegativeOrZero(event: any) {
        return (event.charCode != 8 && event.charCode == 0 || (event.charCode >= 49 && event.charCode <= 57));
    }

    private initFrom() {
        this.addForm = new FormGroup({});
        Object.keys(this.model).forEach(el => {
            this.addForm.addControl(el, this.getControl(el));
        });
    }

    private getControl(key: string): FormControl {
        const attribute = this.attributes.find(a => a.name === key);
        const validators = attribute?.isNotNull || attribute?.isPrimaryKey ? [Validators.required] : [];
        return new FormControl(this.model[key], validators);
    }
}
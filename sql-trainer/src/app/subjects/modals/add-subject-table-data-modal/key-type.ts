import { ISelectOption } from "src/app/models/select-option.modal";

export interface IKeyType {
    key: string,
    type: string,
    selects: ISelectOption[] | null
}
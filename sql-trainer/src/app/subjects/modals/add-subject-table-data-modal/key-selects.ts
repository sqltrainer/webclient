import { ISelectOption } from "src/app/models/select-option.modal";

export interface IKeySelects {
    key: string,
    selects: ISelectOption[]
}
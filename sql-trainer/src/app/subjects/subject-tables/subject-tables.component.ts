import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { ActivatedRoute, Router } from "@angular/router";
import { AddEditSubjectTableModalComponent } from "../modals/add-edit-subject-table-modal/add-edit-subject-table-modal.component";
import { ITable } from "src/app/models/subjects/table.model";
import { TablesService } from "src/services/subjects-info/tables-service";
import { DatabasesService } from "src/services/subjects-info/databases-service";
import { ITableUiDto } from "src/app/models/subjects/table.ui.dto";

@Component({
    selector: 'subject-tables',
    templateUrl: './subject-tables.component.html',
    styleUrls: ['./subject-tables.component.less',
        '../../../styles/styles.less',
        '../../../styles/menu.less',]
})

export class SubjectTablesComponent implements OnInit {
    private isLoadedTables: boolean = false;

    private databaseId: string;
    public databaseName: string;
    public tables: ITable[];
    public tablesDataSource: MatTableDataSource<ITableUiDto>;
    public columns: string[] = ['actions'];

    constructor(
        private tablesService: TablesService, 
        private databasesService: DatabasesService,
        private router: Router, 
        private route: ActivatedRoute,
        private subjectTable: MatDialog,
        private addSubjectTable: MatDialog) { }

    ngOnInit(): void {
        this.databaseId = this.route.snapshot.params['id'];
        
        this.databasesService.getById(this.databaseId).subscribe(database => {
            this.databaseName = database?.name ?? 'null';
        });

        this.refreshTables();
    }

    public showTableData(element: any) {
        this.router.navigate(['subject-table-data', element.id]);
    }

    public goToSubjects(): void {
        this.router.navigate(['subjects']);
    }

    public openSubjectTableModal(element: any): void {
        const dialogRef = this.subjectTable.open(AddEditSubjectTableModalComponent, {
            width: 'fit-content',
            height: '80%',
            data: this.tables.find(table => {
                return table.id === element.id;
            })
        });

        dialogRef.componentInstance.databaseId = this.databaseId;

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshColumns();
            this.isLoadedTables = false;
            this.refreshTables();
        })
    }

    public delete(element: any): void {
        this.tablesService.delete(element.id).subscribe(_ => {
            this.refreshTables();
        });
    }

    public openAddModal(): void {
        const dialogRef = this.addSubjectTable.open(AddEditSubjectTableModalComponent, {
            width:'80%',
            data: this.databaseId
        });
        dialogRef.componentInstance.databaseId = this.databaseId;

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshColumns();
            this.isLoadedTables = false;
            this.refreshTables();
        })
    }

    private refreshColumns(): void {
        this.columns = ['actions'];
    }

    private refreshTables(): void {
        this.tablesService.getByDatabaseId(this.databaseId).subscribe(tables => {
            this.tables = tables;

            const uiTables = this.tables.map(table => {
                return {
                    id: table.id,
                    name: table.name
                }
            });

            this.tablesDataSource = new MatTableDataSource(uiTables);

            if (!this.isLoadedTables) {
                this.mapDataColumns(uiTables);
                this.isLoadedTables = true;
            }
        });
    }

    private mapDataColumns(tables: ITableUiDto[]) {
        if (tables.length <= 0) 
            return;

        const table = tables[0] as any;
        this.columns.unshift(...Object.keys(table).filter(key => {
            return !key.includes('id') && !key.includes('Id')
        }));
    }
}
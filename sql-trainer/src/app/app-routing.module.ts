import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TestsComponent } from './tests/tests.component';
import { QuestionsComponent } from './questions/questions.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { SubjectTablesComponent } from './subjects/subject-tables/subject-tables.component';
import { SubjectTableData } from './subjects/subject-table-data/subject-table-data.component';
import { ThemesComponent } from './themes/themes.component';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { TestComponent } from './schedule-tests/test/test.component';
import { ScheduleTestsComponent } from './schedule-tests/schedule-tests.component';
import { ScheduleTestResultsComponent } from './schedule-tests/test-results/schedule-test-results.component';
import { ScheduleTestResultComponent } from './schedule-tests/test-results/test-result/schedule-test-result.component';

const routes: Routes = [
  { path: 'tests', component: TestsComponent },
  { path: 'themes', component: ThemesComponent },
  { path: 'questions', component: QuestionsComponent },
  { path: 'subjects', component: SubjectsComponent },
  { path: 'subjects/:id', component: SubjectTablesComponent },
  { path: 'subject-table-data/:id', component: SubjectTableData },
  { path: 'users', component: UsersComponent },
  { path: 'groups', component: GroupsComponent },
  { path: 'test/:id', component: TestComponent },
  { path: 'schedule-tests', component: ScheduleTestsComponent },
  { path: 'schedule-test-results/:id', component: ScheduleTestResultsComponent },
  { path: 'schedule-test-result/:testId/:userId', component: ScheduleTestResultComponent },
  { path: 'schedule-test-result/:testId', component: ScheduleTestResultComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

import { MatDialogRef } from '@angular/material/dialog';
import { UsersService } from 'src/services/users-info/users-service';

@Component({
    templateUrl: 'sign-in.component.html',
    styleUrls: [
        '../../styles/dialog.less',
        './sign-in.component.less'
    ]
})
export class SignInDialogComponent implements OnInit {
    public signInForm: FormGroup;
    public isLoginExist: boolean;
    public loginMinCharacters: number = 6;
    public passwordMinCharacters: number = 8;

    constructor(private userService: UsersService, public dialogRef: MatDialogRef<SignInDialogComponent>) { }

    public ngOnInit(): void {
        this.initForm();
    }

    public signIn(): void {
        if (this.signInForm.invalid)
            return;
        
        const registerDto = {
            name: this.signInForm.get('name')?.value,
            login: this.signInForm.get('login')?.value,
            password: this.signInForm.get('passwords')?.get('password')?.value
        }

        this.userService.register(registerDto).subscribe(_ => {
            this.cancel();
        }, err => {
            alert(err.message);
        })
    }

    public cancel(): void {
        this.dialogRef.close();
    }

    private initForm() {
        this.signInForm = new FormGroup({
            name: new FormControl('', [Validators.required]),
            login: new FormControl('', [Validators.required, Validators.minLength(this.loginMinCharacters)]),
            passwords: new FormBuilder().group({
                password: ['', [Validators.required, Validators.minLength(this.passwordMinCharacters)]],
                confirm: ['', [Validators.required, Validators.minLength(this.passwordMinCharacters)]]
            }, { validator: this.checkPasswords })
        });
    }

    public checkLogin(): void {
        if (this.signInForm.get('login')?.invalid)
            return;

        let login = this.signInForm.get('login')?.value;
        this.userService.checkLogin(login).subscribe(checkLoginDto => {
            if (checkLoginDto.isUserFound)
                this.signInForm.get('login')?.setErrors({ loginExists: true });
        })
    }

    private checkPasswords(group: AbstractControl): void {
        let password = group.get('password')?.value;
        let confirmPass = group.get('confirm')?.value
        if (password !== confirmPass) {
            group.get('confirm')?.setErrors({ notPasswordMatch: true });
        }
    }
}
import { ILanguage } from "./language.model";
import { ITable } from "./table.model";

export interface IDatabase {
    id: string,
    name: string,
    languageId: string,
    language: ILanguage | null,
    tables: ITable[] | null
}
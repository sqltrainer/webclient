import { ITable } from "./table.model";

export interface IForeignKey {
    id: string,
    name: string,
    tableId: string,
    table: ITable
}
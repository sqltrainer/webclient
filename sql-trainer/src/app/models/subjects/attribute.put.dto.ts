export interface IAttributePutDto {
    id: string | null,
    type: string,
    varcharNumberOfSymbols: number | null,
    isPrimaryKey: boolean,
    isNotNull: boolean,
    isUnique: boolean,
    order: number,
    defaultValue: string | null,
    foreignKeyId: string | null,
    tableId: string
}
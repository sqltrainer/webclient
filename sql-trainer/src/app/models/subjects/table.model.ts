import { IAttribute } from "./attribute.model";

export interface ITable {
    id: string,
    name: string,
    databaseId: string,
    attributes: IAttribute[]
}
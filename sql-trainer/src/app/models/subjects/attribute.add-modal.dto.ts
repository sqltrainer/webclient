export interface IAttributeAddModalDto {
    id: string,
    name: string,
    type: string,
    varcharNumber: number|null,
    primaryKey: boolean,
    notNull: boolean,
    unique: boolean,
    default: string|null,
    foreignKeyId: string|null,
    order: number
}
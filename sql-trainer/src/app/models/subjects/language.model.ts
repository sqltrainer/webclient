export interface ILanguage {
    id: string,
    name: string,
    codeMirrorName: string
}
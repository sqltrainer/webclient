import { IAttributePutDto } from "./attribute.put.dto";

export interface ITablePutDto {
    id: string,
    name: string,
    databaseId: string,
    attributes: IAttributePutDto[]
}
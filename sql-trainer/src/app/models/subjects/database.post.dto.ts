export interface IDatabasePostDto {
    name: string,
    languageId: string
}
export interface IAttributePostDto {
    name: string,
    type: string,
    varcharNumberOfSymbols: number | null,
    isPrimaryKey: boolean,
    isNotNull: boolean,
    isUnique: boolean,
    order: number,
    defaultValue: string | null,
    foreignKeyId: string | null
}
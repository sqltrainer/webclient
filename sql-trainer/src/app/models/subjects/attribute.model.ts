import { IForeignKey } from "./foreign-key.model";
import { ITable } from "./table.model";

export interface IAttribute {
    id: string,
    name: string,
    type: string,
    varcharNumberOfSymbols: number|null,
    isPrimaryKey: boolean,
    isNotNull: boolean,
    isUnique: boolean,
    defaultValue: string|null,
    foreignKeyId: string|null,
    foreignKey: IForeignKey|null,
    tableId: string,
    table: ITable|null,
    order: number
}
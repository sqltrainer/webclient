import { IAttributePostDto } from "./attribute.post.dto";

export interface ITablePostDto {
    name: string,
    databaseId: string,
    attributes: IAttributePostDto[]
}
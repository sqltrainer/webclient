export interface IQuestionPostDto {
    body: string,
    complexity: number,
    answerBody: string,
    topicId: string,
    databaseId: string
}
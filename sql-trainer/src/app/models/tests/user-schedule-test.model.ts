import { IUser } from "../users/user.model";

export interface IUserScheduleTest {
    userId: string,
    user: IUser | null,
    scheduleTestId: string,
    finishedAt: Date | null,
    checkedByTeacher: boolean | null,
    programMark: number,
    teacherMark: number,
    questionMaxMark: number,
}
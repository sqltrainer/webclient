import { ITestQuestion } from "./test-question.post.put.dto";

export interface ITestPutDto {
    id: string,
    name: string,
    questions: ITestQuestion[]
}
export interface IUserScheduleTestPutDto {
    userId: string | null,
    scheduleTestId: string,
    finishedAt: Date | null,
    checkedByTeacher: boolean | null
}
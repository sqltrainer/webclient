import { ITest } from "./test.model";
import { IUserScheduleTest } from "./user-schedule-test.model";

export interface IScheduleTest {
    id: string,
    userScheduleTests: IUserScheduleTest[],
    testId: string,
    startAt: Date,
    finishAt: Date,
    test: ITest | null
}
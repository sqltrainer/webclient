export interface IQuestionPutDto {
    id: string,
    body: string,
    complexity: number,
    answerBody: string,
    topicId: string,
    databaseId: string
}
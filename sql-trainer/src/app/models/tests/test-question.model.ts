export interface ITestQuestion {
    testId: string,
    questionId: string,
    maxMark: number
}
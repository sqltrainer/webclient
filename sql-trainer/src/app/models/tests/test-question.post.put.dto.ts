export interface ITestQuestion {
    questionId: string,
    maxMark: number
}
import { ITestQuestion } from "./test-question.model";

export interface ITest {
    id: string,
    name: string,
    createdAt: Date,
    testQuestions: ITestQuestion[]
}
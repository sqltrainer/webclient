export interface ICorrectAnswer {
    body: string,
    questionId: string
}
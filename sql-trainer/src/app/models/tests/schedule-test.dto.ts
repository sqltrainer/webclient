export interface IScheduleTestDto {
    id: string,
    userIds: string[],
    testId: string,
    startAt: Date,
    finishAt: Date
}
import { ITest } from "./test.model";

export interface IUserTest {
    userId: string,
    testId: string,
    startAt: Date,
    finishAt: Date,
    test: ITest
}
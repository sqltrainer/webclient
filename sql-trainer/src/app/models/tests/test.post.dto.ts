import { ITestQuestion } from "./test-question.post.put.dto";

export interface ITestPostDto {
    name: string,
    createdAt: Date,
    questions: ITestQuestion[]
}
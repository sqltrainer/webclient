import { IQuestion } from "./question.model";

export interface IUserAnswer {
    scheduleTestId: string;
    questionId: string;
    userId: string;
    body: string;
    programMark: number;
    teacherMark: number;
    questionMaxMark: number;
    answeredAt: Date | null;
    question: IQuestion | null;
}
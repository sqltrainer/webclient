import { IDatabase } from "../subjects/database.model";
import { ICorrectAnswer } from "./correct-answer.model";
import { ITheme } from "./theme.model";

export interface IQuestion {
    id: string,
    body: string,
    complexity: number,
    topicId: string,
    databaseId: string,
    correctAnswer?: ICorrectAnswer,
    topic?: ITheme,
    database?: IDatabase | null
}
export interface ISelectOption {
    id: string,
    value: string,
    children?: ISelectOption[]
}
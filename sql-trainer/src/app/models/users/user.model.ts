export interface IUser {
    id: string | null,
    login: string;
    name: string;
    password: string | null;
    faceImage: string | null;
    rate: number;
    roleId: string;
    groupId: string | null;
}
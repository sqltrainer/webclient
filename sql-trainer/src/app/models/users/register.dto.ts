export interface IRegisterDto {
    name: string,
    login: string,
    password: string
}
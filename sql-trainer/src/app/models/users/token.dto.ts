export interface ITokenDto {
    token: string,
    role: string
}
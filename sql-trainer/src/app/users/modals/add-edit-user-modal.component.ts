import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ISelectOption } from 'src/app/models/select-option.modal';
import { IUser } from 'src/app/models/users/user.model';
import { LocalStorageService } from 'src/services/local-storage/local-storage-service';
import { GroupsService } from 'src/services/users-info/groups-service';
import { RolesService } from 'src/services/users-info/roles.service';
import { UsersService } from 'src/services/users-info/users-service';

@Component({
    templateUrl: 'add-edit-user-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './add-edit-user-modal.component.less'
    ]
})
export class AddEditUserModalComponent implements OnInit {
    public addEditForm: FormGroup;
    public isLoginExist: boolean;
    public loginMinCharacters: number = 6;
    public passwordMinCharacters: number = 8;

    public roleOptions: ISelectOption[];
    public groupOptions: ISelectOption[];

    private studentRoleId: string | null;

    public imageSrc: string;

    constructor(
        private usersService: UsersService,
        private groupsService: GroupsService,
        private rolesService: RolesService,
        private localStorageService: LocalStorageService,
        public dialogRef: MatDialogRef<AddEditUserModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IUser | null) {
    }

    ngOnInit(): void {
        if (this.localStorageService.isAdmin()) {
            this.rolesService.getAll().subscribe(roles => {
                this.studentRoleId = roles.find(r => r.name === 'Student')?.id ?? null;
                this.roleOptions = roles.map(r => { return { id: r.id, value: r.name }});
                this.refreshForm();
            });
        } else if (this.localStorageService.isTeacher()) {
            this.rolesService.getStudent().subscribe(role => {
                this.studentRoleId = role.id;
                this.refreshForm();
            })
        }

        this.groupsService.getAll().subscribe(groups => {
            this.groupOptions = groups.map(g => { return { id: g.id, value: g.name }});
        });

        this.initForm();
    }
    
    public addEditUser(): void {
        if (this.addEditForm.invalid)
            return;

        const user = { ...this.addEditForm.value };
        const roleId = this.localStorageService.isAdmin() ? user.roleId as string : this.studentRoleId as string;
        const userModel = {
            id: this.data === null ? null : this.data.id,
            name: user.name as string,
            login: user.login as string,
            password: this.data === null ? user.password as string : null,
            roleId: roleId,
            groupId: roleId === this.studentRoleId ? user.groupId as string : null,
            rate: this.data === null ? 0.0 : this.data.rate,
            faceImage: user.fileSource,
        }

        if (this.data === null) {
            this.usersService.add(userModel).subscribe(_ => {
                this.close();
            });
        } else {
            this.usersService.update(userModel).subscribe(_ => {
                this.close();
            })
        }
    }

    public close(): void {
        this.dialogRef.close();
    }

    public checkLogin(): void {
        if (this.addEditForm.get('login')?.invalid)
            return;

        let login = this.addEditForm.get('login')?.value;
        this.usersService.checkLogin(login).subscribe(checkLoginDto => {
            if (checkLoginDto.isUserFound)
                this.addEditForm.get('login')?.setErrors({ loginExists: true });
        });
    }

    public changeRole(): void {
        this.refreshForm();
    }

    public shouldShowGroupOptions(): boolean {
        if (this.localStorageService.isTeacher()) {
            return true;
        }

        const role = this.addEditForm.get('roleId');
        return role?.value === this.studentRoleId;
    }

    public shouldShowRoleOptions(): boolean {
        return this.localStorageService.isAdmin();
    }

    public openInput(): void {
        document.getElementById("fileInput")?.click();
    }

    public fileChange(files: FileList): void {
        if (!files || !files.length) 
            return;

        const file = files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            this.imageSrc = reader.result as string;
            this.addEditForm.patchValue({
                fileSource: reader.result
            });
        };
    }

    private initForm(): void {
        this.addEditForm = new FormGroup({
            name: new FormControl(this.data?.name ?? '', [Validators.required]),
            login: new FormControl(this.data?.login ?? '', [Validators.required, Validators.minLength(this.loginMinCharacters)]),
        });

        if (this.localStorageService.isAdmin()) {
            this.addEditForm.setControl('roleId', new FormControl(this.data?.roleId ?? '', [Validators.required]));
        }
        
        if (this.data === null) {
            this.addEditForm.setControl('password', new FormControl('', [Validators.required, Validators.minLength(this.passwordMinCharacters)]));
        }

        this.refreshForm();
    }

    private refreshForm(): void {
        if (this.shouldShowGroupOptions()) {
            this.addEditForm.setControl('groupId', new FormControl(this.data?.groupId, [Validators.required]));
            this.addEditForm.setControl('file', new FormControl('', []));
            this.addEditForm.setControl('fileSource', new FormControl(this.data?.faceImage, [Validators.required]));
            this.imageSrc = this.data?.faceImage ?? '';
        } else {
            this.addEditForm.removeControl('groupId');
            this.addEditForm.removeControl('file');
            this.addEditForm.removeControl('fileSource');
        }
    }
}
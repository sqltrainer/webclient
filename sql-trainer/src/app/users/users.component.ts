import { Component, OnInit } from "@angular/core";
import { IUser } from "../models/users/user.model";
import { UsersService } from "src/services/users-info/users-service";
import { AddEditUserModalComponent } from "./modals/add-edit-user-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";

@Component({
    templateUrl: './users.component.html',
    styleUrls: [
        '../../styles/menu.less',
        './users.component.less'
    ]
})
export class UsersComponent implements OnInit {
    public users: IUser[];

    constructor(
        private usersService: UsersService, 
        private localStorageService: LocalStorageService,
        public addEditUserModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshUsers();
    }

    openAdd(): void {
        this.openAddEdit();
    }

    openEdit(user: IUser): void {
        this.openAddEdit(user);
    }

    openDelete(user: IUser): void {
        this.usersService.delete(user).subscribe(_ => {
            this.refreshUsers();
        });
    }

    private openAddEdit(user: IUser | null = null): void {
        const dialogRef = this.addEditUserModal.open(AddEditUserModalComponent, {
            disableClose: true,
            data: user
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshUsers();
        });
    }

    refreshUsers(): void {
        if (this.localStorageService.isTeacher()) {
            this.usersService.getStudents().subscribe(users => {
                this.users = users;
            })
        } else if (this.localStorageService.isAdmin()) {
            this.usersService.getAll().subscribe(users => {
                this.users = users;
            });
        }
    }
}
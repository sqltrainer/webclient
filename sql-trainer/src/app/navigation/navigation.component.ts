import { Component, Input, OnInit } from "@angular/core";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";

@Component({
    selector: 'navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.less']
})
export class NavigationComponent implements OnInit {
    @Input() public isOpened: boolean;

    constructor(private localStorageService: LocalStorageService) {
    }

    ngOnInit(): void {
    }

    isStudent(): boolean {
        return this.localStorageService.isStudent();
    }

    isTeacher(): boolean {
        return this.localStorageService.isTeacher();
    }

    isAdmin(): boolean {
        return this.localStorageService.isAdmin();
    }
}
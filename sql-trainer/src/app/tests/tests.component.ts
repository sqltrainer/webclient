import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { TestsService } from "src/services/test-info/tests-service";
import { ITest } from "../models/tests/test.model";
import { AddEditTestModalComponent } from "./modals/add-edit-test-modal.component";
import { ConfirmModel, ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";
import { Router } from "@angular/router";

@Component({
    templateUrl: './tests.component.html',
    styleUrls: [
        '../../styles/menu.less',
        './tests.component.less'
    ]
})
export class TestsComponent implements OnInit {
    public tests: ITest[];

    constructor(private testService: TestsService, 
        public addTestModal: MatDialog, 
        public confirmationModal: MatDialog) {
    }

    ngOnInit(): void {
        this.refreshTests();
    }

    openAdd(): void {
        this.openAddEditModal();
    }

    openEdit(test: ITest): void {
        this.openAddEditModal(test);
    }

    openDelete(test: ITest): void {
        const dialogRef = this.confirmationModal.open(ConfirmationModalComponent, {
            maxWidth: '400px',
            disableClose: true,
            data: new ConfirmModel('Confirm action', `Are you sure that you want to delete "${test.name}" test?`)
        })
        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.testService.delete(test.id).subscribe(_ => { this.refreshTests(); });
            }
        })
    }

    private openAddEditModal(test: ITest | null = null): void {
        const dialogRef = this.addTestModal.open(AddEditTestModalComponent, {
            disableClose: true,
            data: test
        });

        dialogRef.afterClosed().subscribe(_ => {
            this.refreshTests();
        })
    }

    private refreshTests(): void {
        this.testService.getAll().subscribe(tests => {
            this.tests = tests;
        });
    }
}
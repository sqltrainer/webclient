import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ISelectOption } from 'src/app/models/select-option.modal';
import { ITest } from 'src/app/models/tests/test.model';
import { Column } from 'src/app/shared/models/table-column';
import { TableModel } from 'src/app/shared/models/table-model';
import { QuestionsService } from 'src/services/test-info/questions-service';
import { TestsService } from 'src/services/test-info/tests-service';
import { ThemesService } from 'src/services/test-info/themes-service';
import { ThemePalette } from '@angular/material/core';

@Component({
    templateUrl: 'add-edit-test-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './add-edit-test-modal.component.less'
    ]
})
export class AddEditTestModalComponent implements OnInit {
    public testForm: FormGroup;

    public themeOptions: ISelectOption[] = [
    ]
    public dataSource: MatTableDataSource<TableModel>;
    public questionColumns: Column[] = [
        { key: 'id', type: 'string', isVisible: false },
        { key: 'checked', type: 'boolean', isVisible: true },
        { key: 'body', type: 'string', isVisible: true },
        { key: 'maxMark', type: 'number', isVisible: true },
        { key: 'Actions', type:'action', isVisible: true }]
    public questionOptions: ISelectOption[] = [
    ]

    constructor(
        private questionsService: QuestionsService,
        private testsService: TestsService,
        private themesService: ThemesService,
        public dialogRef: MatDialogRef<AddEditTestModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ITest | null) {
    }

    ngOnInit(): void {
        this.themesService.getAll().subscribe(res => {
            this.themeOptions = res.map(el => {
                return { id: el.id, value: el.name };
            })
        });
        this.questionsService.getAll().subscribe(questions => {
            this.dataSource = new MatTableDataSource(questions.map(q => {
                const checked = this.data === null || !this.data.testQuestions ? false : this.data.testQuestions.map(dq => dq.questionId).includes(q.id);
                const maxMark = this.data === null || !this.data.testQuestions ? 0 : this.data.testQuestions.find(tq => tq.questionId === q.id)?.maxMark ?? 0;
                return new TableModel({ id: q.id, checked: checked, body: q.body, maxMark: maxMark }, true, false, false)
            }))

            this.questionOptions = questions.map<ISelectOption>(question => {
                return {
                    id: question.id,
                    value: question.body
                }
            });
        });

        this.initForm();
    }

    public isButtonDisabled(): boolean {
        return this.testForm.invalid;
    }

    public addEditTest() {
        if (this.testForm.invalid)
            return;

        const test = { ...this.testForm.value };
        const testModel = {
            id: this.data?.id ?? '',
            name: test.name as string,
            createdAt: new Date(),
            questions: this.dataSource.data
                .filter(d => d.data.checked)
                .map(d => {
                    return {
                        questionId: d.data.id,
                        maxMark: d.data.maxMark
                    };
                })
        };

        if (this.data === null) {
            this.testsService.add(testModel).subscribe(_ => {
                this.close();
            })
        }
        else {
            this.testsService.update(testModel).subscribe(_ => {
                this.close();
            })
        }
    }

    public close(): void {
        this.dialogRef.close();
    }

    private initForm() {
        this.testForm = new FormGroup({
            name: new FormControl(this.data?.name ?? '', [Validators.required])
        });
    }
}
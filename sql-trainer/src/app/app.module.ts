import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDividerModule } from '@angular/material/divider';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule  } from '@angular-material-components/datetime-picker';
import { CustomNgxDateTimeModule } from 'src/services/ngx-datetime-adapter';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTreeModule } from '@angular/material/tree';
import { CountdownModule } from 'ngx-countdown';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LogInDialogComponent } from './log-in/log-in.component';
import { SignInDialogComponent } from './sign-in/sign-in.component';
import { TestsComponent } from './tests/tests.component';
import { QuestionsComponent } from './questions/questions.component';
import { ThemesComponent } from './themes/themes.component';
import { UsersComponent } from './users/users.component';
import { AddEditUserModalComponent } from './users/modals/add-edit-user-modal.component';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddEditTestModalComponent } from './tests/modals/add-edit-test-modal.component';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { MatTableModule } from '@angular/material/table';
import { EntityTableComponent } from './shared/entity-table/entity-table.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { SubjectTablesComponent } from './subjects/subject-tables/subject-tables.component';
import { SubjectTableData } from './subjects/subject-table-data/subject-table-data.component';
import { AddSubjectModalComponent } from './subjects/modals/add-subject-modal/add-subject-modal.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AddEditSubjectTableModalComponent } from './subjects/modals/add-edit-subject-table-modal/add-edit-subject-table-modal.component';
import { AddSubjectTableDataModelComponent } from './subjects/modals/add-subject-table-data-modal/add-subject-table-data-modal.component';
import { ConfirmationModalComponent } from './shared/confirmation-modal/confirmation-modal.component';
import { CreateUpdateThemeModalComponent } from './themes/modals/create-update-theme-modal.component';
import { AddEditQuestionModalComponent } from './questions/modals/add-edit-question-modal.component';
import { GroupsComponent } from './groups/groups.component';
import { AddEditGroupModalComponent } from './groups/modals/add-edit-group-modal.component';
import { ScheduleTestsComponent } from './schedule-tests/schedule-tests.component';
import { AddEditScheduleTestModalComponent } from './schedule-tests/modals/add-edit-schedule-test-modal.component';
import { ScheduleTestResultsComponent } from './schedule-tests/test-results/schedule-test-results.component';
import { ScheduleTestResultComponent } from './schedule-tests/test-results/test-result/schedule-test-result.component';

import { UsersService } from 'src/services/users-info/users-service';
import { TestsService } from 'src/services/test-info/tests-service';
import { QuestionsService } from 'src/services/test-info/questions-service';
import { APIHeadersInterceptor } from './shared/interceptors/api-headers.interceptor';
import { LocalStorageService } from 'src/services/local-storage/local-storage-service';
import { RefreshTokenInterceptor } from './shared/interceptors/refresh-token.interceptor';
import { DatabasesService } from 'src/services/subjects-info/databases-service';
import { TablesService } from 'src/services/subjects-info/tables-service';
import { ThemesService } from 'src/services/test-info/themes-service';
import { LanguagesService } from 'src/services/subjects-info/languages-service';
import { GroupsService } from 'src/services/users-info/groups-service';
import { RolesService } from 'src/services/users-info/roles.service';
import { TestComponent } from './schedule-tests/test/test.component';
import { AnswersService } from 'src/services/test-info/answers-service';
import { ScheduleTestsService } from 'src/services/test-info/schedule-tests-service';
import { FaceVerificationModalComponent } from './shared/face-verification-modal/face-verification-modal.component';
import { WebcamModule } from 'ngx-webcam';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    LogInDialogComponent,
    SignInDialogComponent,
    AddEditQuestionModalComponent,
    AddEditTestModalComponent,
    TestsComponent,
    QuestionsComponent,
    EntityTableComponent,
    SubjectsComponent,
    SubjectTablesComponent,
    SubjectTableData,
    AddSubjectModalComponent,
    AddEditSubjectTableModalComponent,
    AddSubjectTableDataModelComponent,
    ConfirmationModalComponent,
    ThemesComponent,
    CreateUpdateThemeModalComponent,
    UsersComponent,
    AddEditUserModalComponent,
    GroupsComponent,
    AddEditGroupModalComponent,
    TestComponent,
    ScheduleTestsComponent,
    AddEditScheduleTestModalComponent,
    ScheduleTestResultsComponent,
    ScheduleTestResultComponent,
    FaceVerificationModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatDividerModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    TextFieldModule,
    MatSelectModule,
    MatTableModule,
    FormsModule,
    ReactiveFormsModule,
    CodemirrorModule,
    MatCheckboxModule,
    HttpClientModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    CustomNgxDateTimeModule,
    MatExpansionModule,
    MatTreeModule,
    CountdownModule,
    WebcamModule,
    MatProgressSpinnerModule
  ],
  exports: [
    EntityTableComponent
  ],
  providers: [
    UsersService,
    TestsService,
    QuestionsService,
    DatabasesService,
    TablesService,
    LocalStorageService,
    ThemesService,
    LanguagesService,
    GroupsService,
    RolesService,
    AnswersService,
    ScheduleTestsService,
    {
      useClass: APIHeadersInterceptor,
      provide: HTTP_INTERCEPTORS,
      multi: true
    }, 
    {
      useClass: RefreshTokenInterceptor,
      provide: HTTP_INTERCEPTORS,
      multi: true
    }
  ],
  entryComponents: [
    AddEditQuestionModalComponent,
    AddEditTestModalComponent,
    AddSubjectModalComponent,
    AddEditSubjectTableModalComponent,
    AddSubjectTableDataModelComponent,
    ConfirmationModalComponent,
    CreateUpdateThemeModalComponent,
    FaceVerificationModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

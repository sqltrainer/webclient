import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef } from "@angular/material/dialog";
import { ITheme } from "src/app/models/tests/theme.model";
import { IThemePost } from "src/app/models/tests/theme.post.model";
import { IThemePut } from "src/app/models/tests/theme.put.model";
import { ThemesService } from "src/services/test-info/themes-service";

@Component({
    selector: 'app-create-update-theme',
    templateUrl: 'create-update-theme-modal.component.html',
    styleUrls: [
        '../../../styles/dialog.less',
        './create-update-theme-modal.component.less'
    ]
})

export class CreateUpdateThemeModalComponent implements OnInit {
    public theme: ITheme;
    public form: FormGroup;
    public isEditMode: boolean;

    constructor(public dialogRef: MatDialogRef<CreateUpdateThemeModalComponent>, private themesService: ThemesService) { }

    ngOnInit(): void {
        !!this.theme ? this.init(this.theme) : this.init();
    }

    private init(theme?: ITheme) {
        this.isEditMode = !!theme;

        this.form = new FormGroup({
            id: new FormControl(this.theme ? this.theme.id : ''),
            name: new FormControl(this.theme ? this.theme.name : '', [Validators.required])
        });
    }

    public handleAction() {
        this.isEditMode ? this.updateTheme() : this.createTheme();
    }

    public close() {
        this.dialogRef.close();
    }

    private createTheme() {
        if (this.form.invalid) {
            return;
        }

        const theme: IThemePost = {
            name: this.form.get('name')?.value
        };

        this.themesService.createTheme(theme).subscribe(() => {
            this.dialogRef.close(true);
        });
    }

    private updateTheme() {
        if (this.form.invalid) {
            return;
        }

        const theme: IThemePut = {
            id: this.theme.id,
            name: this.form.get('name')?.value
        };

        this.themesService.updateTheme(theme).subscribe(() => {
            this.isEditMode = false;
            this.dialogRef.close(true);
        });
    }
}
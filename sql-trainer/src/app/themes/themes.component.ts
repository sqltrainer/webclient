import { Component, OnInit } from "@angular/core";
import { ThemesService } from "src/services/test-info/themes-service";
import { ITheme } from "../models/tests/theme.model";
import { MatDialog } from "@angular/material/dialog";
import { CreateUpdateThemeModalComponent } from "./modals/create-update-theme-modal.component";
import { ConfirmModel, ConfirmationModalComponent } from "../shared/confirmation-modal/confirmation-modal.component";

@Component({
    selector: 'themes',
    templateUrl: 'themes.component.html',
    styleUrls: [
        '../../styles/styles.less',
        'themes.component.less',
        '../../styles/menu.less',
    ]
})

export class ThemesComponent implements OnInit {
    public themes: ITheme[] = [];

    constructor(private themesService: ThemesService, private dialog: MatDialog, public confirmationModal: MatDialog) { }

    ngOnInit(): void {
        this.getAllThemes();
    }

    public openAdd() {
        const dialogRef = this.dialog.open(CreateUpdateThemeModalComponent, {
            minWidth: '400px'
        });
        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.getAllThemes();
            }
        })
    }

    public openEdit(theme: ITheme) {
        const dialogRef = this.dialog.open(CreateUpdateThemeModalComponent, {
            minWidth: '400px'
        });

        dialogRef.componentInstance.theme = theme;

        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.getAllThemes();
            }
        })
    }

    public openDelete(theme: ITheme) {
        const dialogRef = this.confirmationModal.open(ConfirmationModalComponent, {
            maxWidth: '400px',
            disableClose: true,
            data: new ConfirmModel('Confirm action', `Are you sure that you want to delete theme?`)
        })
        dialogRef.afterClosed().subscribe((res) => {
            if (res) {
                this.themesService.deleteTheme(theme.id).subscribe(_ => {
                    this.getAllThemes();
                });
            }
        })
    }

    private getAllThemes() {
        this.themesService.getAll().subscribe((res) => {
            this.themes = res;
        });
    }
}
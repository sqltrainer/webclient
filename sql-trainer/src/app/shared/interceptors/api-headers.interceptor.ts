import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";

@Injectable()
export class APIHeadersInterceptor implements HttpInterceptor {

    constructor(private localStorageService: LocalStorageService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = this.localStorageService.getToken();

        if (!!token && req.url !== 'https://localhost:7144/api/users/login' && req.url !== 'https://localhost:7144/api/users/refresh-token') {
            req = req.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }

        if (!(req.body instanceof FormData)) {
            req.headers.set("Content-Type", "application/json");
        }

        return next.handle(req);
    }
}
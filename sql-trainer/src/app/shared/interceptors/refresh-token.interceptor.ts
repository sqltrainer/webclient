import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, catchError, filter, switchMap, take, throwError } from "rxjs";
import { LocalStorageService } from "src/services/local-storage/local-storage-service";
import { UsersService } from "src/services/users-info/users-service";

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
    private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    constructor(private userService: UsersService, private localStorageService: LocalStorageService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(catchError(error => {
            if (error instanceof HttpErrorResponse && error.status === 401 && req.url !== 'https://localhost:7144/api/users/login') {
                return this.handle401Error(req, next);
            }
            return throwError(error);
        }))
    }

    private handle401Error(req: HttpRequest<any>, next: HttpHandler) {
        let refreshToken = this.localStorageService.getRefreshToken();

        if (!!refreshToken) {
            return this.userService.refreshToken(refreshToken).pipe(switchMap(token => {
                this.localStorageService.saveToken(token);
                this.refreshTokenSubject.next(token);

                return next.handle(this.setHeaders(req, token));
            }),
                catchError((err) => {
                    return throwError(err);
                }));
        }

        return this.refreshTokenSubject.pipe((filter(token => token !== null)),
            take(1),
            switchMap((token => next.handle(this.setHeaders(req, token))))
        );
    }

    private setHeaders(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({
            setHeaders: {
                Authorization: `Bearer ${token}`
            }
        })
    }
}
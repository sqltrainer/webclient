import { Component, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { WebcamImage } from 'ngx-webcam';
import { Subject } from 'rxjs';
import { IUserVerify } from 'src/app/models/users/user-verify.model';
import { UsersService } from 'src/services/users-info/users-service';

@Component({
  selector: 'app-face-verification-modal',
  templateUrl: './face-verification-modal.component.html',
  styleUrls: ['./face-verification-modal.component.less',
    '../../../styles/dialog.less']
})
export class FaceVerificationModalComponent {
  @ViewChild("WebcamComponent", { static: false }) webcam: any;

  public readonly imageTrigger: Subject<void> = new Subject<void>();
  public isImageVerifying: boolean = false;

  public errors: string[] = [];

  constructor(public dialogRef: MatDialogRef<FaceVerificationModalComponent>,
    private usersService: UsersService) { }

  public captureImage(webcamImage: WebcamImage): void {
    let imageAsUrl = webcamImage.imageAsDataUrl;
    let imageAsBase64 = webcamImage.imageAsBase64;

    let userVerify: IUserVerify = {
      base64: imageAsBase64,
      extension: this.getImageExtension(imageAsUrl)
    };

    this.isImageVerifying = true;
    this.usersService.verifyUser(userVerify).subscribe(res => {
      if (res.verified) {
        this.dialogRef.close(res);
      } else {
        this.isImageVerifying = false;
        this.errors = res.errors;
        if (!this.errors) {
          this.errors = [ 'Verification failed!' ]
        }
      }
    })
  }

  public triggerSnapshot(): void {
    this.imageTrigger.next();
  }

  public close() {
    this.dialogRef.close();
  }

  private getImageExtension(imageAsDataUrl: string): string {
    let indexOfSlash = imageAsDataUrl.indexOf('/');
    let indexOfSemiColon = imageAsDataUrl.indexOf(';');
    return imageAsDataUrl.slice(indexOfSlash, indexOfSemiColon).replace('/', '.');
  }
}

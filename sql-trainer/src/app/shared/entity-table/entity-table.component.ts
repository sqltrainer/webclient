import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { Column } from "../models/table-column";
import { TableModel } from "../models/table-model";
import camelcase from "camelcase";
import { EditTableModel } from "../models/edit-table-model";
import { ThemePalette } from "@angular/material/core";

@Component({
    selector: 'app-entity-table',
    templateUrl: './entity-table.component.html',
    styleUrls: [
        './entity-table.component.less'
    ]
})
export class EntityTableComponent implements OnInit {
    @Input() public columns: Column[];
    @Input() public dataSource: MatTableDataSource<TableModel>;
    @Input() public isShowAdd: boolean = false;
    @Input() public isShowDelete: boolean = true;

    @Output() public onEditColumn: EventEmitter<TableModel> = new EventEmitter<TableModel>();
    @Output() public onDeleteColumn: EventEmitter<TableModel> = new EventEmitter<TableModel>();
    @Output() public onShowMore: EventEmitter<TableModel> = new EventEmitter<TableModel>();
    @Output() public onShowAddRow: EventEmitter<TableModel> = new EventEmitter<TableModel>();
    
    private oldData: any;
    public minDate: Date;
    public color: ThemePalette = 'primary';

    constructor(public addQuestionModal: MatDialog) {
    }

    ngOnInit(): void {
    }
    
    editElement(element: TableModel) {
        element.isEdit = false;
        const editElement = element as EditTableModel;
        for (let column of this.columns) {
            if (column.type === 'date') {
                element.data[column.key] = element.data[column.key].toDate();
            }
        }
        editElement.oldData = this.oldData;
        this.onEditColumn.emit(editElement);
    }

    deleteColumn(element: TableModel) {
        this.onDeleteColumn.emit(element);
    }

    startColumnEditing(element: TableModel) {
        this.oldData = structuredClone(element.data);
        element.isEdit = !element.isEdit;
    }

    getVisibleColumnsKeys() {
        return this.columns.filter(el => el.isVisible).map(col => {
            return col.key;
        })
    }

    showMore(element: TableModel) {
        this.onShowMore.emit(element);
    }

    onAddClick() {
        this.onShowAddRow.emit();
    }

    addElement(element: TableModel) {
        element.isAdd = false;
    }

    toPascaleCase(key: string): string {
        return camelcase(key, { pascalCase: true })
    }

    getOptionValue(column: Column, dataValue: any) {
        return column.customColumnOptions?.find(el => el.id == dataValue)?.value;
    }

    public formatDate(date: Date): string {
        const dateTime = new Date(date);
        return this.padLeft(dateTime.getDate(), 2, '0') + '-' +
            this.padLeft(dateTime.getMonth() + 1, 2, '0') + '-' +
            dateTime.getFullYear() + ' ' +
            this.padLeft(dateTime.getHours(), 2, '0') + ':' + 
            this.padLeft(dateTime.getMinutes(), 2, '0');
    }

    private padLeft(value: string|number, length: number, character: string): string {
        return String(value).padStart(length, character);
    }
}


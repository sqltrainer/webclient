import { ISelectOption } from "src/app/models/select-option.modal";

export class Column {
    key: string;
    type: string;
    isVisible: boolean;
    customColumnOptions?: ISelectOption[];

    constructor(key: string, type: string, isVisible: boolean) {
        this.key = key;
        this.type = type;
        this.isVisible = isVisible;
    }
}
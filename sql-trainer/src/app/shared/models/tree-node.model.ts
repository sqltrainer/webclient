export interface ITreeNode { 
    name: string,
    childrens: ITreeNode[] | null
}

export interface INode {
    expandable: boolean;
    name: string;
    level: number;
}

export interface IAttributeNode extends INode {
    isPrimaryKey: boolean;
    isForeignKey: boolean;
}

export interface IAttributeTreeNode extends ITreeNode {
    isPrimaryKey: boolean,
    isForeignKey: boolean
}
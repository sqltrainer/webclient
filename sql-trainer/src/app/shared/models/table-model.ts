export class TableModel {
    data: any;
    isEdit: boolean;
    isAdd?: boolean;
    isShowMore?: boolean;

    constructor(data: any, isEdit: boolean, isShowMore?:boolean, isAdd?: boolean) {
        this.data = data;
        this.isEdit = isEdit;
        this.isShowMore = isShowMore;
        this.isAdd = isAdd;
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, catchError, throwError } from 'rxjs';
import { ILoginDto } from '../../app/models/users/login.dto';
import { ICheckLoginDto } from '../../app/models/users/check-login.dto';
import { ITokenDto } from 'src/app/models/users/token.dto';
import { ILoginModel } from 'src/app/models/users/login.model';
import { IUser } from 'src/app/models/users/user.model';
import { IRegisterDto } from 'src/app/models/users/register.dto';
import { LocalStorageService } from '../local-storage/local-storage-service';
import { IUserVerify } from 'src/app/models/users/user-verify.model';

@Injectable()
export class UsersService {
    private baseUri: string = 'https://localhost:7144/api/users/';

    public loginStatus: Subject<ILoginModel> = new Subject<ILoginModel>();

    constructor(private localStorageService: LocalStorageService, private http: HttpClient) {}

    public getLoginStatusEvent(): Observable<ILoginModel> {
        return this.loginStatus.asObservable();
    }

    public getUser(): Observable<IUser> {
        return this.http.get<IUser>(this.baseUri + 'by-id').pipe(
            catchError(err => {
                this.localStorageService.clearLocalStorage();
                return throwError(err);
            }));
    }

    public getById(userId: string): Observable<IUser> {
        return this.http.get<IUser>(this.baseUri + userId);
    }

    public refreshToken(refreshToken: string) : Observable<string> {
        return this.http.post<string>(this.baseUri + 'refresh-token', refreshToken);
    }

    public logIn(loginModel: ILoginDto): Observable<ITokenDto> {
        return this.http.post<ITokenDto>(this.baseUri + 'login', loginModel);
    }

    public checkLogin(login: string): Observable<ICheckLoginDto> {
        return this.http.get<ICheckLoginDto>(this.baseUri + 'check/' + login);
    }

    public register(registerDto: IRegisterDto): Observable<any> {
        return this.http.post(this.baseUri + 'register', registerDto)
    }

    public getAll(): Observable<IUser[]> {
        return this.http.get<IUser[]>(this.baseUri);
    }

    public add(user: IUser): Observable<any> {
        return this.http.post(this.baseUri, user);
    }

    public update(user: IUser): Observable<any> {
        return this.http.put(this.baseUri, user);
    }

    public delete(user: IUser): Observable<any> {
        return this.http.delete(this.baseUri + user.id);
    }

    public getStudents(): Observable<IUser[]> {
        return this.http.get<IUser[]>(this.baseUri + 'students');
    }

    public getStudentsWithoutGroup(): Observable<IUser[]> {
        return this.http.get<IUser[]>(this.baseUri + 'students-wo-group')
    }

    public getStudentsByGroupOrWithoutGroup(groupId: string): Observable<IUser[]> {
        return this.http.get<IUser[]>(this.baseUri + 'students-by-group-or-wo-group/' + groupId);
    }

    public verifyUser(userVerify: IUserVerify) : Observable<any> {
        return this.http.post(this.baseUri + 'verify', userVerify);
    }
}

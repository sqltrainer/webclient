import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRole } from 'src/app/models/users/role.model';

@Injectable()
export class RolesService {
    private baseUri: string = 'https://localhost:7144/api/roles/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IRole[]> {
        return this.http.get<IRole[]>(this.baseUri);
    }

    public getStudent(): Observable<IRole> {
        return this.http.get<IRole>(this.baseUri + 'student');
    }
}
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IGroup } from 'src/app/models/users/group.model';

@Injectable()
export class GroupsService {
    private baseUri: string = 'https://localhost:7144/api/groups/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IGroup[]> {
        return this.http.get<IGroup[]>(this.baseUri);
    }

    public add(group: IGroup): Observable<any> {
        return this.http.post(this.baseUri, group);
    }

    public update(group: IGroup): Observable<any> {
        return this.http.put(this.baseUri, group);
    }

    public delete(group: IGroup): Observable<any> {
        return this.http.delete(this.baseUri + group.id);
    }
}

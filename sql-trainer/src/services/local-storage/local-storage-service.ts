import { Injectable } from "@angular/core";

@Injectable()
export class LocalStorageService {
    public saveToken(token: string): void {
        localStorage.setItem('token', token);
    }

    public getToken() : string | null {
        return localStorage.getItem('token');
    }

    public saveRefreshToken(refreshToken: string): void {
        localStorage.setItem('refresh-token', refreshToken);
    }

    public getRefreshToken(): string | null {
        return localStorage.getItem('refresh-token');
    }

    public saveRole(role: string): void {
        return localStorage.setItem('role', role);
    }

    public isAdmin(): boolean {
        return this.getRole() === 'Administrator';
    }

    public isTeacher(): boolean {
        return this.getRole() === 'Teacher';
    }

    public isStudent(): boolean {
        return this.getRole() === 'Student';
    }

    public getRole(): string | null {
        return localStorage.getItem('role');
    }

    public addQuestionAnswer(questionId: string, answerBody: string): void {
        localStorage.setItem(questionId, answerBody);
    }

    public removeQuestionAnswer(questionId: string): void {
        localStorage.removeItem(questionId);
    }

    public getQuestionAnswer(questionId: string): string | null {
        return localStorage.getItem(questionId);
    }

    public setCurrentQuestionIndex(index: number): void {
        localStorage.setItem('currentQuestionIndex', index.toString());
    }

    public getCurrentQuestionIndex(): number | null {
        const value = localStorage.getItem('currentQuestionIndex');
        return !value ? null : parseInt(value);
    }

    public saveCountOfVerificationFails(count: number): void {
        localStorage.removeItem('verificationFailures');
        localStorage.setItem('verificationFailures', count.toString());
    }

    public getCountOfVerificationFails(): number | null {
        const value = localStorage.getItem('verificationFailures');
        return !value ? null : parseInt(value);
    }

    public clearLocalStorage() {
        localStorage.clear();
    }
}
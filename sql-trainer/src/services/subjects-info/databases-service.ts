import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, map } from "rxjs";
import { IAttribute } from "src/app/models/subjects/attribute.model";
import { IDatabase } from "src/app/models/subjects/database.model";
import { IDatabasePostDto } from "src/app/models/subjects/database.post.dto";
import { IDatabasePutDto } from "src/app/models/subjects/database.put.dto";

@Injectable()
export class DatabasesService {
    private baseUri: string = 'https://localhost:7195/api/databases/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IDatabase[]> {
        return this.http.get<IDatabase[]>(this.baseUri).pipe(map(data => {
            return data.map(db => {
                return {
                    id: db.id,
                    name: db.name,
                    languageId: db.languageId,
                    language: null,
                    tables: null
                }
            })
        }));
    }

    public getFullAll(): Observable<IDatabase[]> {
        return this.http.get<IDatabase[]>(this.baseUri + 'full').pipe(map(data => {
            return data.map(db => {
                return {
                    id: db.id,
                    name: db.name,
                    languageId: db.languageId,
                    tables: db.tables,
                    language: db.language
                }
            });
        }));
    }
    
    public add(databaseDto: IDatabasePostDto) : Observable<any> {
        return this.http.post(this.baseUri, databaseDto);
    }

    public update(databaseDto: IDatabasePutDto): Observable<any> {
        return this.http.put(this.baseUri, databaseDto);
    }

    public delete(id: string): Observable<any> {
        return this.http.delete(this.baseUri + id);
    }

    public getById(id: string): Observable<IDatabase|null> {
        return this.http.get<IDatabase>(this.baseUri + id).pipe(map(data => {
            if (data === null) 
                return null;
                
            return {
                id: data.id,
                name: data.name,
                languageId: data.languageId,
                tables: null,
                language: null
            }
        }));
    }

    public getFullById(id: string): Observable<IDatabase> {
        return this.http.get<IDatabase>(this.baseUri + 'full/' + id).pipe(map(data => {
            return {
                id: data.id,
                name: data.name,
                languageId: data.languageId,
                tables: data.tables,
                language: data.language
            }
        }));
    }

    public getPrimaryKeysByDatabaseId(databaseId: string): Observable<IAttribute[]> {
        return this.http.get<IAttribute[]>(this.baseUri + 'primary-keys/' + databaseId);
    }
    
    public execute(databaseId: string, body: string): Observable<any> {
        return this.http.post(this.baseUri + 'execute-script/' + databaseId, {
            script: body
        });
    }
}
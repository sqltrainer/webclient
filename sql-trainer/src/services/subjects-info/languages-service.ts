import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError } from "rxjs";
import { ILanguage } from "src/app/models/subjects/language.model";

@Injectable()
export class LanguagesService {
    private url: string = 'https://localhost:7195/api/languages/'

    constructor(private http: HttpClient) { }

    public getAll(): Observable<ILanguage[]> {
        return this.http.get<ILanguage[]>(this.url).pipe(
            catchError((error) => {
                throw error;
            })
        );
    }
}
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IAttribute } from "src/app/models/subjects/attribute.model";
import { ITable } from "src/app/models/subjects/table.model";
import { ITablePostDto } from "src/app/models/subjects/table.post.dto";
import { ITablePutDto } from "src/app/models/subjects/table.put.dto";

@Injectable()
export class TablesService {
    private baseUri: string = 'https://localhost:7195/api/tables/';

    constructor(private http: HttpClient) {}

    public getByDatabaseId(databaseId: string): Observable<ITable[]> {
        return this.http.get<ITable[]>(this.baseUri + 'by-database-id/' + databaseId);
    }

    public add(table: ITablePostDto): Observable<any> {
        return this.http.post(this.baseUri, table);
    }

    public update(table: ITablePutDto): Observable<any> {
        return this.http.put(this.baseUri, table);
    }

    public getById(id: string): Observable<ITable> {
        return this.http.get<ITable>(this.baseUri + id);
    }

    public delete(id: string): Observable<any> {
        return this.http.delete(this.baseUri + id);
    }

    public getAttributesByTableId(tableId: string): Observable<IAttribute[]> {
        return this.http.get<IAttribute[]>(this.baseUri + 'attributes/' + tableId);
    }

    public getAttributesByPrimaryKey(attributeId: string) {
        return this.http.get<IAttribute[]>(this.baseUri + 'attributes-by-id/' + attributeId);
    }

    public getDataByTableId(tableId: string): Observable<any> {
        return this.http.get<any>(this.baseUri + 'table-data/' + tableId);
    }

    public getDataByForeignKeyId(foreignKeyId: string): Observable<any> {
        return this.http.get<any>(this.baseUri + 'table-data-by-foreign-key/' + foreignKeyId);
    }

    public insertDataIntoTable(tableId: string, data: any): Observable<any> {
        return this.http.post(this.baseUri + 'table-data/' + tableId, data);
    }

    public deleteData(tableId: string, data: any): Observable<any> {
        return this.http.delete(this.baseUri + 'table-data/' + tableId, {
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }), 
            body: data
        });
    }

    public updateData(tableId: string, data: any): Observable<any> {
        return this.http.put(this.baseUri + 'table-data/' + tableId, data);
    }
}
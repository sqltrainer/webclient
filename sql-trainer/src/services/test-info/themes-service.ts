import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, catchError } from "rxjs";
import { ITheme } from "src/app/models/tests/theme.model";
import { IThemePost } from "src/app/models/tests/theme.post.model";
import { IThemePut } from "src/app/models/tests/theme.put.model";

@Injectable()
export class ThemesService {
    private url: string = 'https://localhost:7076/api/topics/'
    
    constructor(private http: HttpClient) { }

    public getAll(): Observable<ITheme[]> {
        return this.http.get<ITheme[]>(this.url).pipe(
            catchError((error) => {
                throw error;
            })
        );
    }

    public getTheme(id: string): Observable<any> {
        return this.http.get(`${this.url}${id}`).pipe(
            catchError((error) => {
                throw error;
            })
        )
    }

    public createTheme(theme: IThemePost): Observable<any> {
        return this.http.post(this.url, theme).pipe(
            catchError((error) => {
                throw error;
            })
        );
    }

    public updateTheme(theme: IThemePut): Observable<any> {
        return this.http.put(this.url, theme).pipe(
            catchError((error) => {
                throw error;
            })
        );
    }

    public deleteTheme(id: string): Observable<any> {
        return this.http.delete(`${this.url}${id}`).pipe(
            catchError((error) => {
                throw error;
            })
        );
    }
}
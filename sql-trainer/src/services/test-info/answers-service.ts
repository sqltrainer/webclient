import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { ICheckAnswerDto } from "src/app/models/tests/check-answer.dto";
import { LocalStorageService } from "../local-storage/local-storage-service";
import { Injectable } from "@angular/core";
import { IUserAnswer } from "src/app/models/tests/user-answer.model";

@Injectable()
export class AnswersService {
    private readonly baseUri: string = 'https://localhost:7076/api/answers/';

    constructor(private httpClient: HttpClient, private localStorageService: LocalStorageService) {
    }

    public checkQuery(questionId: string, query: string): Observable<ICheckAnswerDto> {
        return this.httpClient.post<ICheckAnswerDto>(this.baseUri + 'check/' + questionId, {
            body: query,
            token: this.localStorageService.getToken()
        });
    }

    public addAnswers(answers: IUserAnswer[]): Observable<any> {
        return this.httpClient.post(this.baseUri, { answers: answers, token: this.localStorageService.getToken() });
    }

    public getStudentAnswers(scheduleTestId: string): Observable<IUserAnswer[]> {
        return this.httpClient.get<IUserAnswer[]>(this.baseUri + 'by-student/' + scheduleTestId);
    }
    
    public getUserAnswers(userId: string, scheduleTestId: string): Observable<IUserAnswer[]> {
        return this.httpClient.get<IUserAnswer[]>(this.baseUri + userId + '/' + scheduleTestId);
    }
    
    public setTeacherMark(answer: IUserAnswer): Observable<any> {
        return this.httpClient.put(this.baseUri, answer);
    }
}
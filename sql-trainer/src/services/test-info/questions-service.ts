import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IQuestion } from "src/app/models/tests/question.model";
import { IQuestionPostDto } from "src/app/models/tests/question.post.dto";
import { IQuestionPutDto } from "src/app/models/tests/question.put.dto";

@Injectable()
export class QuestionsService {
    private baseUri: string = 'https://localhost:7076/api/questions/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IQuestion[]> {
        return this.http.get<IQuestion[]>(this.baseUri).pipe();
    }

    public getQuestionsByTestId(testId: string): Observable<IQuestion[]> {
        return this.http.get<IQuestion[]>(this.baseUri + testId);
    }

    public getQuestionsByScheduleTestId(scheduleTestId: string): Observable<IQuestion[]> {
        return this.http.get<IQuestion[]>(this.baseUri + 'by-schedule-test-id/' + scheduleTestId);
    }

    public add(question: IQuestionPostDto): Observable<any> {
        return this.http.post(this.baseUri, question);
    }

    public update(question: IQuestionPutDto): Observable<any> {
        return this.http.put(this.baseUri, question);
    }

    public delete(id: string): Observable<any> {
        return this.http.delete(this.baseUri + id);
    }
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ITest } from "src/app/models/tests/test.model";
import { ITestPostDto } from "src/app/models/tests/test.post.dto";
import { ITestPutDto } from "src/app/models/tests/test.put.dto";

@Injectable()
export class TestsService {
    private baseUri: string = 'https://localhost:7076/api/tests/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<ITest[]> {
        return this.http.get<ITest[]>(this.baseUri).pipe();
    }

    public getTestById(testId: string): Observable<ITest> {
        return this.http.get<ITest>(`${this.baseUri}${testId}`);
    }

    public add(test: ITestPostDto): Observable<any> {
        return this.http.post(this.baseUri, test);
    }

    public update(test: ITestPutDto): Observable<any> {
        return this.http.put(this.baseUri, test);
    }

    public delete(testId: string): Observable<any> {
        return this.http.delete(this.baseUri + testId);
    }
}

import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IScheduleTestDto } from "src/app/models/tests/schedule-test.dto";
import { IScheduleTest } from "src/app/models/tests/schedule-test.model";
import { IUserScheduleTestPutDto } from "src/app/models/tests/user-schedule-test.put.dto";

@Injectable()
export class ScheduleTestsService {
    private baseUri: string = 'https://localhost:7076/api/scheduletests/';

    constructor(private http: HttpClient) {}

    public getAll(): Observable<IScheduleTest[]> {
        return this.http.get<IScheduleTest[]>(this.baseUri);
    }

    public getAllByUserId(): Observable<IScheduleTest[]> {
        return this.http.get<IScheduleTest[]>(this.baseUri + 'by-user-id');
    }

    public add(test: IScheduleTestDto): Observable<any> {
        return this.http.post(this.baseUri, test);
    }

    public update(test: IScheduleTestDto): Observable<any> {
        return this.http.put(this.baseUri, test);
    }
    
    public getById(scheduleTestId: string): Observable<IScheduleTest> {
        return this.http.get<IScheduleTest>(this.baseUri + scheduleTestId);
    }
    
    public updateFinished(userScheduleTest: IUserScheduleTestPutDto): Observable<any> {
        return this.http.put(this.baseUri + 'finish', userScheduleTest);
    }
    
    public delete(id: string): Observable<any> {
        return this.http.delete(this.baseUri + id);
    }
}
